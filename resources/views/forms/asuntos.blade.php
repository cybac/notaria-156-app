@extends('layouts.admin')
@section('titulo', $form_edit ? "Editar Asunto" : "Nuevo Asunto")
@section('content')
<div style="margin: 70px 15px 0px; padding-top: 50px; padding-bottom: 70px;">
    <div class="page-header">
        <h3 class="page-title">{{ $form_edit ?"Editar Asunto" :"Nuevo Asunto" }}</h3>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div>
                <div>
                    <div class="alert alert-warning"> Los campos con <span class="text-danger">*</span> son requeridos</div>
                        <form action="{{ $form_edit ? route('actualizar_asunto', $asunto->id) : route('guardar_asunto') }}" method="POST" class="form_files row" id="form_folio" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group col-6 col-md-6">
                                <label for="nombre">Nombre <span class="text-danger">*</span></label>
                                <input type="text" class="form-control form-control-lg" name="nombre" id="nombre" value="{{ $form_edit ? $asunto->nombre : "" }}">
                                <span class="invalid-feedback"></span>
                            </div>
                            <div class="form-group col-6 col-md-6">
                                <label for="valoracion">Ponderación: <span id="ponderacion">{{ $form_edit ? $asunto->valoracion : "50" }}</span></label>
                                <input type="range" class="form-control form-control-lg ponderacion" name="valoracion" id="valoracion" value="{{ $form_edit ? $asunto->valoracion : "50" }}" min="1" max="100">
                                <span class="invalid-feedback"></span>
                            </div>
                            <div class="form-group text-center col-12">
                                <button class="btn btn-success btn-lg submit" type="submit">{{ $form_edit ?"Actualizar" :"Guardar" }}</button>
                                <a href="{{ route('asuntos') }}" class="btn btn-danger btn-lg">Cancelar</a>
                            </div>     
                        </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{asset('js/valoracion.js')}}"></script>
@endsection
