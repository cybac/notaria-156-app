@extends('layouts.admin')
@section('titulo', $form_edit ? "Editar Folio" : "Asignar Folio")
@section('content')
<div style="margin: 70px 15px 0px; padding-top: 50px; padding-bottom: 70px;">
    <div class="page-header">
        <h3 class="page-title">{{ $form_edit ?"Editar Folio" :"Asignar Folio" }}</h3>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div>
                <div>
                        <form action="{{ $form_edit ? route('actualizar_folio', $expediente->id) : route('guardar_folio', $expediente) }}" method="POST" class="form_files row" id="form_folio" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group col-12 col-md-12">
                                <label for="expediente" style="display: none;">Expediente</label>
                                <input type="text" class="form-control form-control-lg" name="expediente" id="expediente" value="{{ $expediente }}" style="display: none;">
                            </div>
                            <div class="form-group col-4 col-md-4">
                                <label for="escritura">No. Escritura</label>
                                <input type="text" class="form-control form-control-lg" name="escritura" id="escritura" value="{{ $form_edit ? $folio->escritura : "" }}">
                                <span class="invalid-feedback"></span>
                            </div>
                            <div class="form-group col-4 col-md-4">
                                <label for="volumen">Volumen</label>
                                <input type="text" class="form-control form-control-lg" name="volumen" id="volumen" value="{{ $form_edit ? $folio->volumen : "" }}">
                                <span class="invalid-feedback"></span>
                            </div>
                            <div class="form-group col-2 col-md-2">
                                <label for="folio_ini">Folio Inicial</label>
                                <input type="text" class="form-control form-control-lg" name="folio_ini" id="folio_ini" value="{{ $form_edit ? $folio->folio_inicial : "" }}">
                                <span class="invalid-feedback"></span>
                            </div>
                            <div class="form-group col-2 col-md-2">
                                <label for="folio_fin">Folio Final</label>
                                <input type="text" class="form-control form-control-lg" name="folio_fin" id="folio_fin" value="{{ $form_edit ? $folio->folio_fin : "" }}">
                                <span class="invalid-feedback"></span>
                            </div>
                            <div class="form-group col-12 col-md-12">
                                <label for="actos">Actos</label>
                                <input type="text" class="form-control form-control-lg" name="actos" id="actos" value="{{ $form_edit ? $folio->actos : "" }}">
                                <span class="invalid-feedback"></span>
                            </div>
                            <div class="form-group col-6 col-md-6">
                                <label for="partes">Partes</label>
                                <textarea class="form-control form-control-lg" name="partes" id="partes" cols="30" rows="10" value="{{ $form_edit ? $folio->partes : "" }}"></textarea>
                                <span class="invalid-feedback"></span>
                            </div>
                            <div class="form-group col-6 col-md-6">
                                <label for="notas">Notas</label>
                                <textarea class="form-control form-control-lg" name="notas" id="notas" cols="30" rows="10" value="{{ $form_edit ? $folio->notas : "" }}"></textarea>
                                <span class="invalid-feedback"></span>
                            </div>
                            <div class="form-group text-center col-12">
                                <button class="btn btn-success btn-lg submit" type="submit">{{ $form_edit ?"Actualizar" :"Guardar" }}</button>
                                <a href="{{ route('folios', $expediente) }}" class="btn btn-danger btn-lg">Cancelar</a>
                            </div>     
                        </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
