@extends('layouts.admin')
@section('titulo', "Nuevo Pago")
@section('content')
<div style="margin: 70px 15px 0px; padding-top: 50px; padding-bottom: 70px;">
    <div class="page-header">
        <h3 class="page-title">{{ $form_edit ?"Edición de los datos del pago" :"Registrar Nuevo Pago" }}</h3>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div>
                <div>
                    <div class="alert alert-warning"> Los campos con <span class="text-danger">*</span> son requeridos</div>
                        <form action="{{ $form_edit ? route('actualizar_pago', $pago->id) : route('guardar_pago') }}" method="POST" class="form_files row" id="form_cliente" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group col-12 col-md-6" style="display: none;">
                                <label for="cliente">Cliente <span class="text-danger">*</span></label>
                                <input type="text" class="form-control form-control-lg" name="cliente" id="cliente" value="{{$cliente}}">
                                <span class="invalid-feedback"></span>
                            </div>
                            <div class="form-group col-12 col-md-6">
                                <label for="nombre">Nombre <span class="text-danger">*</span></label>
                                <input type="text" class="form-control form-control-lg" name="nombre" id="nombre" value="{{ $form_edit ? $pago->nombre : "" }}">
                                <span class="invalid-feedback"></span>
                            </div>
                            <div class="form-group col-12 col-md-6">
                                <label for="monto">Monto <span class="text-danger">*</span></label>
                                <input type="number" step="0.01" min="0" class="form-control form-control-lg" name="monto" id="monto" value="{{ $form_edit ? $pago->monto : "" }}">
                                <span class="invalid-feedback"></span>
                            </div>
                            <div class="form-group col-12 col-md-12">
                                <label for="descripcion">Descripción <span class="text-danger">*</span></label>
                                <textarea class="form-control form-control-lg" name="descripcion" id="descripcion" cols="30" rows="10" >{{ $form_edit ? $pago->descripcion : "" }}</textarea>
                                <span class="invalid-feedback"></span>
                            </div>
                            <div class="form-group text-center col-12">
                                <button class="btn btn-success btn-lg submit" type="submit">{{ $form_edit ?"Actualizar" :"Guardar" }}</button>
                                <a href="{{ route('pagos', $cliente) }}" class="btn btn-danger btn-lg">Cancelar</a>
                            </div>     
                        </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{asset('js/bloquear_e.js')}}"></script>
@endsection
