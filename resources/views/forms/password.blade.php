@extends('layouts.admin')
@section('titulo', "Perfil")
@section('content')
<div style="margin: 70px 15px 0px; padding-top: 50px; padding-bottom: 70px;">
    <div class="row">
        <div class="col-lg-12">
            <div>
                <div>
                    <form action="{{ route('actualizar_password') }}" method="POST" class="form_files row" id="form_perfil" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group col-12">
                            <label for="password">Contraseña Actual </label>
                            <input type="password" class="form-control form-control-lg" name="password" id="password">
                            <span class="invalid-feedback"></span>
                        </div>
                        <div class="form-group col-12">
                            <label for="new_password">Nueva Contraseña </label>
                            <input type="password" class="form-control form-control-lg" name="new_password" id="new_password">
                            <span class="invalid-feedback"></span>
                        </div>
                        <div class="form-group col-12">
                            <label for="conf_password">Confirmar Nueva Contraseña </label>
                            <input type="password" class="form-control form-control-lg" name="conf_password" id="conf_password">
                            <span class="invalid-feedback"></span>
                        </div>
                        <div class="form-group text-center col-12">
                            <button class="btn btn-success btn-lg submit" type="submit">Actualizar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
