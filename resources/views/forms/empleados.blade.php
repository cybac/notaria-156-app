@extends('layouts.admin')
@section('titulo', $form_edit ? "Edición de los datos del empleado" : "Nuevo Empleado" )
@section('content')
<div style="margin: 70px 15px 0px; padding-top: 50px; padding-bottom: 70px;">
    <div class="page-header">
        <h3 class="page-title">{{ $form_edit ?"Edición de los datos del empleado" :"Nuevo Empleado" }}</h3>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div>
                <div>
                    <div class="alert alert-warning"> Los campos con <span class="text-danger">*</span> son requeridos</div>
                        <form action="{{ $form_edit ? route('actualizar_empleado', $empleado->id) : route('guardar_empleado') }}" method="POST" class="form_files row" id="form_empleado" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group col-12 col-md-4">
                                <label for="nombre">Nombre <span class="text-danger">*</span></label>
                                <input type="text" class="form-control form-control-lg" name="nombre" id="nombre" value="{{ $form_edit ? $empleado->nombre : "" }}">
                                <span class="invalid-feedback"></span>
                            </div>
                            <div class="form-group col-12 col-md-4">
                                <label for="paterno">Paterno <span class="text-danger">*</span></label>
                                <input type="text" class="form-control form-control-lg" name="paterno" id="paterno" value="{{ $form_edit ? $empleado->paterno : "" }}">
                                <span class="invalid-feedback"></span>
                            </div>
                            
                            <div class="form-group col-12 col-md-4">
                                <label for="materno">Materno <span class="text-danger">*</span></label>
                                <input type="text" class="form-control form-control-lg" id="materno" name="materno" value="{{ $form_edit ? $empleado->materno : "" }}">
                                <span class="invalid-feedback"></span>
                            </div>
                            <div class="form-group col-12 col-md-6">
                                <label for="telefono">Télefono <span class="text-danger">*</span></label>
                                <input type="text" class="form-control form-control-lg" id="telefono" name="telefono" value="{{ $form_edit ? $empleado->telefono : "" }}">
                                <span class="invalid-feedback"></span>
                            </div>
                            <div class="form-group col-12 col-md-6">
                                <label for="rol">Rol <span class="text-danger">*</span></label>
                                <select class="form-control form-control-lg" name="rol" id="rol">
                                    @if($form_edit)
                                    <option value="1" {{($rol == "administrador") ? "selected" : ""}}>Administrador</option>
                                    <option value="2" {{($rol == "notario") ? "selected" : ""}}>Notario</option>
                                    <option value="3" {{($rol == "capturista") ? "selected" : ""}}>Capturista</option>
                                    @else
                                    <option value="1">Administrador</option>
                                    <option value="2" selected>Notario</option>
                                    <option value="3">Capturista</option>
                                    @endif
                                </select>
                            </div>
                            <div class="form-group col-12 col-md-6">
                                <label for="email">Correo <span class="text-danger">*</span></label>
                                <input type="email" class="form-control form-control-lg" id="email" name="email" value="{{ $form_edit ? $usuario->email : "" }}" {{ $form_edit ? "disabled" : "" }}>
                                <span class="invalid-feedback"></span>
                            </div>
                            <div class="form-group col-12 col-md-6">
                                <label for="password">Contraseña <span class="text-danger">*</span></label>
                                <input type="password" class="form-control form-control-lg" id="password" name="password" value="{{ $form_edit ? "**********" : "" }}" {{ $form_edit ? "disabled" : "" }}>
                                <button id="show" name="show" class="Mostrar-p" type="button" {{ $form_edit ? "disabled" : "" }}><i id="show-icon" name="show-icon" class="far fa-eye"></i></button>
                                <span class="invalid-feedback"></span>
                            </div>
                            <div class="form-group text-center col-12">
                                <button class="btn btn-success btn-lg submit" type="submit">{{ $form_edit ?"Actualizar" :"Guardar" }}</button>
                                <a href="{{ route('empleados') }}" class="btn btn-danger btn-lg">Cancelar</a>
                            </div>     
                        </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{asset('js/show.js')}}"></script>
<script src="{{asset('js/telefono.js')}}"></script>
@endsection
