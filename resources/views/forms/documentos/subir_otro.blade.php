@extends('layouts.admin')
@section('titulo', "Nuevo Documento")
@section('content')
<div style="margin: 70px 15px 0px; padding-top: 50px; padding-bottom: 70px;">
    <div class="page-header">
        <h3 class="page-title">{{ $form_edit ?"Edición del documento" :"Nuevo Documento" }}</h3>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div>
                <div>
                    <div class="alert alert-warning"> Los campos con <span class="text-danger">*</span> son requeridos
                    </div>
                    <form action="{{ $form_edit ? route('actualizar_documento', $documento->id) : route('guardar_documento', 3) }}"
                        method="POST" class="form_files row" id="form_expediente" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group col-12 col-md-6" style="display: none;">
                            <label for="expediente"></label>
                            <input type="text" class="form-control form-control-lg" name="expediente" id="expediente" value="{{ $expediente}}">
                            <span class="invalid-feedback"></span>
                        </div>
                        <div class="form-group col-12 col-md-6">
                            <label for="alias">Tipo de documento <span class="text-danger">*</span></label>
                            <input type="text" class="form-control form-control-lg" name="alias" id="alias"
                                value="{{ $form_edit ? $documento->alias : "" }}">
                            <span class="invalid-feedback"></span>
                        </div>

                        <div class="form-group col-12 col-md-6">
                            <label for="documento">Documento <span class="text-danger">*</span></label>
                            <input type="file" name="documento" id="documento" class="form-control form-control-lg">
                            <span class="invalid-feedback"></span>
                        </div>
                        <div class="form-group text-center col-12">
                            <button class="btn btn-success btn-lg submit" type="submit">Guardar</button>
                            <a href="{{ route('otros', $expediente) }}" class="btn btn-danger btn-lg">Cancelar</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
