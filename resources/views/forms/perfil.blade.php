@extends('layouts.admin')
@section('titulo', "Perfil")
@section('content')
<div style="margin: 70px 15px 0px; padding-top: 50px; padding-bottom: 70px;">
    <div class="row">
        <div class="col-lg-12">
            <div>
                <div>
                    <form action="{{ route('actualizar_perfil') }}" method="POST" class="form_files row" id="form_perfil" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group col-12 col-md-6">
                            <label for="nombre">Nombre </label>
                            <input type="text" class="form-control form-control-lg" name="nombre" id="nombre" value="{{$perfil->nombre}}">
                            <span class="invalid-feedback"></span>
                        </div>
                        <div class="form-group col-12 col-md-6">
                            <label for="paterno">Apellido Paterno </label>
                            <input type="text" class="form-control form-control-lg" name="paterno" id="paterno" value="{{$perfil->paterno}}">
                            <span class="invalid-feedback"></span>
                        </div>
                        <div class="form-group col-12 col-md-6">
                            <label for="materno">Apellido Materno </label>
                            <input type="text" class="form-control form-control-lg" name="materno" id="materno" value="{{$perfil->materno}}">
                            <span class="invalid-feedback"></span>
                        </div>
                        <div class="form-group col-12 col-md-6">
                            <label for="telefono">Teléfono </label>
                            <input type="text" class="form-control form-control-lg" name="telefono" id="telefono" value="{{$perfil->telefono}}">
                            <span class="invalid-feedback"></span>
                        </div>
                        <div class="form-group text-center col-12">
                            <button class="btn btn-success btn-lg submit" type="submit">Actualizar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
