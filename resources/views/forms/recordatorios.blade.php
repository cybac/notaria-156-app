@extends('layouts.admin')
@section('titulo', $form_edit ? "Editar Recordatorio" : "Nuevo Recordatorio")
@section('content')
<div style="margin: 70px 15px 0px; padding-top: 50px; padding-bottom: 70px;">
    <div class="page-header">
        <h3 class="page-title">{{ $form_edit ?"Editar Recordatorio" :"Nuevo Recordatorio" }}</h3>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div>
                <div>
                        <form action="{{ $form_edit ? route('actualizar_recordatorio', $recordatorio->id) : route('guardar_recordatorio', $expediente) }}" method="POST" class="form_files row" id="form_folio" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group col-12 col-md-12">
                                <label for="expediente" style="display: none;">Expediente</label>
                                <input type="text" class="form-control form-control-lg" name="expediente" id="expediente" value="{{ $expediente }}" style="display: none;">
                            </div>
                            <div class="form-group col-9 col-md-9">
                                <label for="titulo">Titulo</label>
                                <input type="text" class="form-control form-control-lg" name="titulo" id="titulo" value="{{ $form_edit ? $recordatorio->titulo : "" }}">
                                <span class="invalid-feedback"></span>
                            </div>
                            <div class="form-group col-3 col-md-3">
                                <label for="titulo">Fecha</label>
                                <input type="date" class="form-control form-control-lg" name="fecha" id="fecha" value="{{ $form_edit ? $recordatorio->titulo : date('Y-m-d') }}" min="{{date('Y-m-d')}}" max="{{date('Y-m-d', strtotime(date('Y-m-d')."+ 2 year"))}}">
                                <span class="invalid-feedback"></span>
                            </div>
                            <div class="form-group col-12 col-md-12">
                                <label for="notas">Contenido</label>
                                <textarea class="form-control form-control-lg" name="contenido" id="contenido" cols="30" rows="10" value="{{ $form_edit ? $recordatorio->contenido : "" }}"></textarea>
                                <span class="invalid-feedback"></span>
                            </div>
                            <div class="form-group text-center col-12">
                                <button class="btn btn-success btn-lg submit" type="submit">{{ $form_edit ?"Actualizar" :"Guardar" }}</button>
                                <a href="{{ route('recordatorios', $expediente) }}" class="btn btn-danger btn-lg">Cancelar</a>
                            </div>     
                        </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
