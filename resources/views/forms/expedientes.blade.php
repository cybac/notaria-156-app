@extends('layouts.admin')
@section('titulo', $form_edit ? "Edición del Expediente" : "Nuevo Expediente")
@section('content')
<div style="margin: 70px 15px 0px; padding-top: 50px; padding-bottom: 70px;">
    <div class="page-header">
        <h3 class="page-title">{{ $form_edit ?"Edición del Expediente" :"Nuevo Expediente" }}</h3>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="w-100 text-right form-group">
                <a class="btn btn-success col-md-2" href="{{ route('crear_cliente') }}">Nuevo Cliente</a>
            </div>
            <div>
                <div>
                    <div class="alert alert-warning"> Los campos con <span class="text-danger">*</span> son requeridos</div>
                        <form action="{{ $form_edit ? route('actualizar_expediente', $expedientes->id) : route('guardar_expediente') }}" method="POST" class="form_files row" id="form_expediente" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group col-12 col-md-6">
                                <label for="cliente">Cliente <span class="text-danger">*</span></label>
                                <div class="row col-12">
                                    <select name="tipo" id="tipo" class="form-control form-control-lg col-12 col-md-5 mr-auto mb-2">
                                        <option value="1">Personas Físicas</option>
                                        <option value="2">Personas Morales</option>
                                    </select>
                                    <select name="cliente" id="cliente" class="form-control form-control-lg col-12 col-md-6 mb-2">
                                        @foreach ($clientes as $item)
                                        <option value="{{ $item->id }}" {{ $form_edit && $item->id == $expedientes->cliente_id  ? "selected" : "" }}>{{ $item->nombre." ".$item->paterno." ".$item->materno }}</option>
                                        @endforeach
                                    </select>
                                    <span class="invalid-feedback"></span>
                                </div>
                            </div>
                            <div class="form-group col-12 col-md-6">
                                <label for="asunto">Asunto <span class="text-danger">*</span></label>
                                <select name="asunto" id="asunto" class="form-control form-control-lg">
                                    @foreach ($asuntos as $item)
                                        <option value="{{ $item->id }}" {{ $form_edit && $item->id == $expedientes->asunto_id  ? "selected" : "" }}>{{ $item->nombre." ".$item->paterno." ".$item->materno }}</option>
                                    @endforeach
                                </select>
                                <span class="invalid-feedback"></span>
                            </div>
                            
                            <div class="form-group col-12 col-md-6">
                                <label for="responsable">Responsable <span class="text-danger">*</span></label>
                                <select name="responsable" id="responsable" class="form-control form-control-lg">
                                    @foreach ($empleados as $item)
                                        <option value="{{ $item->id }}" {{ $form_edit && $item->id == $expedientes->empleado_id  ? "selected" : "" }}>{{ $item->nombre." ".$item->paterno." ".$item->materno }}</option>
                                    @endforeach
                                </select>
                                <span class="invalid-feedback"></span>
                            </div>
                            <div class="form-group col-12 col-md-6">
                                <label for="INE">INE</label>
                                <input type="file" name="INE" id="INE" class="form-control form-control-lg">
                                <span class="invalid-feedback"></span>
                            </div>
                            <div class="form-group col-12 col-md-6">
                                <label for="CURP">CURP </label>
                                <input type="file" name="CURP" id="CURP" class="form-control form-control-lg">
                                <span class="invalid-feedback"></span>
                            </div>
                            <div class="form-group col-12 col-md-6">
                                <label for="Nacimiento">Acta de nacimiento</label>
                                <input type="file" name="Nacimiento" id="Nacimiento" class="form-control form-control-lg">
                                <span class="invalid-feedback"></span>
                            </div>
                            <div class="form-group col-12 col-md-6">
                                <label for="CD">Comprobante de domicilio</label>
                                <input type="file" name="CD" id="CD" class="form-control form-control-lg">
                                <span class="invalid-feedback"></span>
                            </div>
                            <div class="form-group col-12 col-md-6">
                                <label for="RFC">RFC</label>
                                <input type="file" name="RFC" id="RFC" class="form-control form-control-lg">
                                <span class="invalid-feedback"></span>
                            </div>
                            <div class="form-group col-12 col-md-6">
                                <label for="propio">Expediente Propio</label>
                                <span class='switch' style="display: block"><input type='checkbox' class='switch' name="propio" id="propio"><label for="propio"></label></span>
                                <span class="invalid-feedback"></span>
                            </div>
                            
                            <div class="form-group text-center col-12">
                                <button class="btn btn-success btn-lg submit" type="submit">Guardar</button>
                                <a href="{{ route('expedientes') }}" class="btn btn-danger btn-lg">Cancelar</a>
                            </div>     
                        </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{asset('js/load_tipo_cliente.js')}}"></script>
@endsection
