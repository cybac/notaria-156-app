@extends('layouts.admin')
@section('titulo', $form_edit ? "Edición de los datos del cliente" :"Nuevo Cliente" )
@section('content')
<div style="margin: 70px 15px 0px; padding-top: 50px; padding-bottom: 70px;">
    <div class="page-header">
        <h3 class="page-title">{{ $form_edit ?"Edición de los datos del cliente" :"Nuevo Cliente" }}</h3>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="alert alert-warning"> Los campos con <span class="text-danger">*</span> son requeridos</div>
            <form action="{{ $form_edit ? route('actualizar_cliente', $cliente->id) : route('guardar_cliente') }}" method="POST" class="form_files row" id="form_cliente" enctype="multipart/form-data">
                @csrf
                <div class="form-group col-12 col-md-12">
                    <label for="tipo">Tipo <span class="text-danger">*</span></label>
                    <select class="form-control form-control-lg" name="tipo" id="tipo">
                        @if ($form_edit)
                            @if ($cliente->razon_social == "Moral")
                            <option value="1" >Persona Física</option>
                            <option value="2" selected>Persona Moral</option>
                            @else
                            <option value="1" selected>Persona Física</option>
                            <option value="2" >Persona Moral</option>
                            @endif
                        @else
                        <option value="1" selected>Persona Física</option>
                        <option value="2">Persona Moral</option>
                        @endif
                    </select>
                </div>
                @if ($form_edit)
                    @if ($cliente->razon_social == "Moral")
                    <div class="form-group col-12 col-md-6 col-md-12" id="nom">
                        <label for="nombre" id="nombre_cliente">Nombre de la empresa <span class="text-danger">*</span></label>
                        <input type="text" class="form-control form-control-lg" name="nombre" id="nombre" value="{{ $cliente->nombre }}">
                        <span class="invalid-feedback"></span>
                    </div>
                    <div class="form-group col-12 col-md-6 d-none" id="pat"></div>
                    <div class="form-group col-12 col-md-6 d-none" id="mat"></div>
                    <div class="form-group col-12 col-md-6 d-none" id="cur"></div>
                    <div class="form-group col-12 col-md-6 col-md-12" id="rf">
                        <label for="rfc">RFC <span class="text-danger">*</span></label>
                        <input type="text" class="form-control form-control-lg" id="rfc" name="rfc" value="{{ $cliente->RFC }}">
                        <span class="invalid-feedback"></span>
                    </div>
                    <div class="form-group col-12 col-md-6 col-md-12" id="tel">
                        <label for="telefono">Télefono <span class="text-danger">*</span></label>
                        <input type="text" class="form-control form-control-lg" id="telefono" name="telefono" value="{{ $cliente->telefono }}">
                        <span class="invalid-feedback"></span>
                    </div>
                    @else
                    <div class="form-group col-12 col-md-6" id="nom">
                        <label for="nombre" id="nombre_cliente">Nombre <span class="text-danger">*</span></label>
                        <input type="text" class="form-control form-control-lg" name="nombre" id="nombre" value="{{ $cliente->nombre }}">
                        <span class="invalid-feedback"></span>
                    </div>
                    <div class="form-group col-12 col-md-6" id="pat">
                        <label for="paterno">Paterno <span class="text-danger">*</span></label>
                        <input type="text" class="form-control form-control-lg" name="paterno" id="paterno" value="{{ $cliente->paterno }}">
                        <span class="invalid-feedback"></span>
                    </div>
                    
                    <div class="form-group col-12 col-md-6" id="mat">
                        <label for="materno">Materno <span class="text-danger">*</span></label>
                        <input type="text" class="form-control form-control-lg" id="materno" name="materno" value="{{ $cliente->materno }}">
                        <span class="invalid-feedback"></span>
                    </div>
                    <div class="form-group col-12 col-md-6" id="cur">
                        <label for="curp">CURP <span class="text-danger">*</span></label>
                        <input type="text" class="form-control form-control-lg" id="curp" name="curp" value="{{ $cliente->CURP }}">
                        <span class="invalid-feedback"></span>
                    </div>
                    <div class="form-group col-12 col-md-6" id="rf">
                        <label for="rfc">RFC <span class="text-danger">*</span></label>
                        <input type="text" class="form-control form-control-lg" id="rfc" name="rfc" value="{{ $cliente->RFC }}">
                        <span class="invalid-feedback"></span>
                    </div>
                    <div class="form-group col-12 col-md-6" id="tel">
                        <label for="telefono">Télefono <span class="text-danger">*</span></label>
                        <input type="text" class="form-control form-control-lg" id="telefono" name="telefono" value="{{ $cliente->telefono }}">
                        <span class="invalid-feedback"></span>
                    </div>
                    @endif
                @else
                <div class="form-group col-12 col-md-6" id="nom">
                    <label for="nombre" id="nombre_cliente">Nombre <span class="text-danger">*</span></label>
                    <input type="text" class="form-control form-control-lg" name="nombre" id="nombre" value="">
                    <span class="invalid-feedback"></span>
                </div>
                <div class="form-group col-12 col-md-6" id="pat">
                    <label for="paterno">Paterno <span class="text-danger">*</span></label>
                    <input type="text" class="form-control form-control-lg" name="paterno" id="paterno" value="">
                    <span class="invalid-feedback"></span>
                </div>
                <div class="form-group col-12 col-md-6" id="mat">
                    <label for="materno">Materno <span class="text-danger">*</span></label>
                    <input type="text" class="form-control form-control-lg" id="materno" name="materno" value="">
                    <span class="invalid-feedback"></span>
                </div>
                <div class="form-group col-12 col-md-6" id="cur">
                    <label for="curp">CURP <span class="text-danger">*</span></label>
                    <input type="text" class="form-control form-control-lg" id="curp" name="curp" value="">
                    <span class="invalid-feedback"></span>
                </div>
                <div class="form-group col-12 col-md-6" id="rf">
                    <label for="rfc">RFC <span class="text-danger">*</span></label>
                    <input type="text" class="form-control form-control-lg" id="rfc" name="rfc" value="">
                    <span class="invalid-feedback"></span>
                </div>
                <div class="form-group col-12 col-md-6" id="tel">
                    <label for="telefono">Télefono <span class="text-danger">*</span></label>
                    <input type="text" class="form-control form-control-lg" id="telefono" name="telefono" value="">
                    <span class="invalid-feedback"></span>
                </div>
                @endif
                
                <div class="form-group text-center col-12">
                    <button class="btn btn-success btn-lg submit" type="submit" >{{ $form_edit ? "Actualizar" :"Guardar" }}</button>
                    <a href="{{ route('clientes') }}" class="btn btn-danger btn-lg">Cancelar</a>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="{{asset('js/telefono.js')}}"></script>
<script src="{{asset('js/tipo_cliente.js')}}"></script>
<script src="{{asset('js/mayusculas.js')}}"></script>
@endsection
