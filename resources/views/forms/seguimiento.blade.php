@extends('layouts.admin')
@section('titulo', $form_edit ? "Editar Folio" : "Asignar Folio")
@section('content')
<div style="margin: 70px 15px 0px; padding-top: 50px; padding-bottom: 70px;">
    <div class="page-header">
        <h3 class="page-title">{{ $form_edit ?"Editar Seguimiento" :"Seguimiento" }}</h3>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div>
                <div>
                    <form
                        action="{{ $form_edit ? route('actualizar_seguimiento', $seguimiento->id) : route('guardar_seguimiento', $expediente) }}"
                        method="POST" class="form_files row" id="form_seguimiento" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group col-12 col-md-12">
                            <label for="id_expediente" style="display: none;">Expediente</label>
                            <input type="text" class="form-control form-control-lg" name="id_expediente"
                                id="id_expediente" value="{{ $form_edit ? $expediente->folio : $expediente }}"
                                style="display: none;">
                        </div>
                        <div class="form-group col-12 col-md-12">
                            <label for="titulo">Titulo</label>
                            <input type="text" class="form-control form-control-lg" name="titulo" id="titulo"
                                value="{{ $form_edit ? $seguimiento->titulo : "" }}">
                            <span class="invalid-feedback"></span>
                        </div>
                        <div class="form-group col-12 col-md-12">
                            <label for="descripcion">Descripción</label>
                            <textarea class="form-control form-control-lg" name="descripcion" id="descripcion" cols="30" rows="10" value="{{ $form_edit ? $seguimiento->descripcion : "" }}"></textarea>
                            <span class="invalid-feedback"></span>
                        </div>
                        <div class="form-group text-center col-12">
                            <button class="btn btn-success btn-lg submit"
                                type="submit">{{ $form_edit ?"Actualizar" :"Guardar" }}</button>
                            <a href="{{ route('seguimiento', $expediente) }}" class="btn btn-danger btn-lg">Cancelar</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
