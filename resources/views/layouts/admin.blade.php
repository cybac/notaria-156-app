<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8" />
    <title>@yield('titulo')</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="HOME" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-barstyle" content="black-translucent">
    <link rel="apple-touch-icon" href="{{asset('images/favicon.ico')}}">
    <meta name="apple-mobile-web-app-title" content="Flatkit">

    <meta name="mobile-web-app-capable" content="yes">
    <link rel="shortcut icon" sizes="196x196" href="{{asset('images/favicon.ico')}}">


    <link rel="stylesheet" href="{{asset('css/animate.css/animate.min.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('css/glyphicons/glyphicons.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('css/font-awesome/css/font-awesome.min.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('css/material-design-icons/material-design-icons.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('css/bootstrap/dist/css/bootstrap.min.css')}}" type="text/css" />

    <link rel="stylesheet" href="{{asset('css/styles/app.css')}}" type="text/css" />

    <link rel="stylesheet" href="{{asset('css/styles/font.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('css/extra.css')}}" type="text/css" />

    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/459fd11905.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.6/css/responsive.dataTables.min.css">
</head>
<body>
    <div class="app" id="app">
        @include('includes.menu')
        <div id="content" class="app-content box-shadow-z0" role="main">
            @include('includes.cabecera')
            @yield('content')
            @include('includes.footer')
        </div>
    </div>
    <script src="{{asset('js/libs/jquery/jquery/dist/jquery.js')}}"></script>

    <script src="{{asset('js/libs/jquery/tether/dist/js/tether.min.js')}}"></script>
    <script src="{{asset('js/libs/jquery/bootstrap/dist/js/bootstrap.js')}}"></script>

    <script src="{{asset('js/libs/jquery/underscore/underscore-min.js')}}"></script>
    <script src="{{asset('js/libs/jquery/jQuery-Storage-API/jquery.storageapi.min.js')}}"></script>
    <script src="{{asset('js/libs/jquery/PACE/pace.min.js')}}"></script>

    <script src="{{asset('js/config.lazyload.js')}}"></script>

    <script src="{{asset('js/palette.js')}}"></script>
    <script src="{{asset('js/ui-load.js')}}"></script>
    <script src="{{asset('js/ui-jp.js')}}"></script>
    <script src="{{asset('js/ui-include.js')}}"></script>
    <script src="{{asset('js/ui-device.js')}}"></script>
    <script src="{{asset('js/ui-form.js')}}"></script>
    <script src="{{asset('js/ui-nav.js')}}"></script>
    <script src="{{asset('js/ui-screenfull.js')}}"></script>
    <script src="{{asset('js/ui-scroll-to.js')}}"></script>
    <script src="{{asset('js/ui-toggle-class.js')}}"></script>
    <script src="{{asset('js/ui-toggle-class.js')}}"></script>
    <script src="{{asset('js/ui-toggle-class.js')}}"></script>

    <script src="{{asset('js/app.js')}}"></script>

    <script src="{{asset('js/libs/jquery/jquery-pjax/jquery.pjax.js')}}"></script>
    <script src="{{asset('js/ajax.js')}}"></script>
    <script src="{{asset('js/tablas.js')}}"></script>
    <script src="{{asset('js/modal.js')}}"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.6/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
</body>