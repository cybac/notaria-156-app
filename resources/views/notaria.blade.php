@extends('layouts.admin')
@section('titulo', "Sistema")
@section('content')
<div class="app" id="app">
    @include('includes.menu')
    <div id="content" class="app-content box-shadow-z0" role="main">
        @include('includes.cabecera')
        @include('includes.footer')
    </div>
</div>
@endsection