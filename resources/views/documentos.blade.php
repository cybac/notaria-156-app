@extends('layouts.admin')

@if ($criterio == 1)
@section('titulo', "Documentos Generales")
@elseif($criterio == 2)
@section('titulo', "Impuesto y derechos")
@else
@section('titulo', "Otros Documentos")
@endif

@section('content')
<div style="margin: 70px 15px 0px; padding-top: 50px; padding-bottom: 70px;">
    <div class="card w-100 p-5">
    @if ($criterio == 1)
    <h3 class="text-center">Documentos Generales</h3>
    @elseif($criterio == 2)
    <h3 class="text-center">Documentos Impuestos y derechos</h3>
    @else
    <h3 class="text-center">Otros Documentos</h3>
    @endif
        <div class="w-100 text-right form-group">
            @if ($criterio == 2)
            <a class="btn btn-success col-md-2" href="{{ route('crear_impuestos_derechos', $expediente) }}">Subir Nuevo</a>
            @elseif($criterio == 3)
            <a class="btn btn-success col-md-2" href="{{ route('crear_otro', $expediente) }}">Subir Nuevo</a>
            @endif
            <a class="btn btn-success col-md-2" href="{{ route('expedientes') }}">Expedientes</a>
        </div>
        <table class="table" id="dt_documentos" data-url="{{ route('dt_documentos', [$expediente, $criterio]) }}" style="width: 100%">
            <thead>
                <tr>
                    <th>Tipo</th>
                    <th>Fecha</th>
                    <th>Status</th>
                    <th>Acciones</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection