@extends('layouts.admin')
@section('titulo', "Asuntos")
@section('content')
<div style="margin: 70px 15px 0px; padding-top: 50px; padding-bottom: 70px;">
    <div class="card w-100 p-5">
        <h3 class="text-center">Asuntos</h3>
        <div class="w-100 text-right form-group">
            <a class="btn btn-success col-md-2" href="{{ route('crear_asunto') }}">Nuevo</a>
        </div>
        <table class="table" id="dt_asuntos" data-url="{{ route('dt_asuntos') }}" style="width: 100%">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Ponderación</th>
                    <th>Acciones</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection