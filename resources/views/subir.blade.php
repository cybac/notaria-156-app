@extends('layouts.admin')
@section('titulo', "Expedientes")
@section('content')
<div style="margin: 70px 15px 0px; padding-top: 50px; padding-bottom: 70px;">
    <div class="card w-100 p-5">
        <h3 class="text-center">Documentos</h3>
        <div class="w-100 text-right form-group">
            <a class="btn btn-success col-md-2" href="{{ route('crear_documentos', $expedientes) }}">Subir Nuevo</a>
            <a class="btn btn-success col-md-2" href="{{ route('expedientes') }}">Atras</a>
        </div>
        <table class="table" id="dt_documentos" data-url="{{ route('dt_documentos', $expedientes) }}" style="width: 100%">
            <thead>
                <tr>
                    <th>Tipo</th>
                    <th>Fecha</th>
                    <th>Status</th>
                    <th>Acciones</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection