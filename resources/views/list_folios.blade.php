@extends('layouts.admin')
@section('titulo', "Folios")
@section('content')
<div style="margin: 70px 15px 0px; padding-top: 50px; padding-bottom: 70px;">
    <div class="card w-100 p-5">
        <h3 class="text-center">Folios</h3>
        @if($all)
        <table class="table" id="dt_listar_folios" data-url="{{ route('dt_listar_folios') }}" style="width: 100%">
        @else
        <table class="table" id="dt_listar_folios_empleado" data-url="{{ route('dt_listar_folios_empleado',$empleado) }}" style="width: 100%">
        @endif
        
            <thead>
                <tr>
                    <th>No. Escritura</th>
                    <th>Volumen</th>
                    <th>Folio</th>
                    <th>Actos</th>
                    <th>Partes</th>
                    <th>Notas</th>
                    @if($all)
                    <th>Empleado</th>
                    @endif
                    <th>Fecha</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection