@extends('layouts.admin')
@section('titulo', "Empleados")
@section('content')
<div style="margin: 70px 15px 0px; padding-top: 50px; padding-bottom: 70px;">
    <div class="card w-100 p-5">
        <h3 class="text-center">Empleados</h3>
        <div class="w-100 text-right form-group">
            <a class="btn btn-success col-md-2" href="{{ route('crear_empleado') }}">Nuevo</a>
        </div>
        <table class="table" id="dt_empleados" data-url="{{ route('dt_empleados') }}" style="width: 100%">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Correo</th>
                    <th>Carga</th>
                    <th>Ponderación General</th>
                    <th>Acciones</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection