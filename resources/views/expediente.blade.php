@extends('layouts.admin')
@section('titulo', "Expedientes del empleado")
@section('content')
<div style="margin: 70px 15px 0px; padding-top: 50px; padding-bottom: 70px;">
    <div class="card w-100 p-5">
        <h3 class="text-center">Expedientes</h3>
        <div class="w-100 text-right form-group">
            <a class="btn btn-success col-md-2" href="{{ route('empleados') }}">Atras</a>
        </div>
        <table class="table" id="dt_expediente" data-url="{{ route('dt_expediente', $empleado) }}" style="width: 100%">
            <thead>
                <tr>
                    <th>Asunto</th>
                    <th>Cliente</th>
                    <th>Fuente</th>
                    <th>Fecha</th>
                    <th>Status</th>
                    <th>Valoración</th>
                    <th>Documentos</th>
                    <th>Acciones</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection