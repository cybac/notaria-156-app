@extends('layouts.admin')
@section('titulo', 'Pagos')
@section('content')
<div style="margin: 70px 15px 0px; padding-top: 50px; padding-bottom: 70px;">
    <div class="card w-100 p-5">
        <h3 class="text-center">Pagos</h3>
        <div class="w-100 text-right form-group">
            <a class="btn btn-success col-md-2" href="{{ route('crear_pago', $cliente) }}">Nuevo Pago</a>
        </div>
        <table class="table" id="dt_pagos" data-url="{{ route('dt_pagos', $cliente) }}" style="width: 100%">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Monto</th>
                    <th>Descripción</th>
                    <th>Fecha</th>
                    <th>Acciones</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

@endsection