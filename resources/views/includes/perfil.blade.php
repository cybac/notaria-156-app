<div class="dropdown-menu dropdown-menu-overlay pull-right">
    <a class="dropdown-item" ui-sref="app.page.profile" href="{{route('perfil')}}">Perfil</a>
    <a class="dropdown-item" ui-sref="app.page.profile" href="{{route('password')}}">Cambiar Contraseña</a>

    <div class="dropdown-divider"></div>
    <a class="dropdown-item" ui-sref="access.signin" href="{{route('logout')}}"
        onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Cerrar Sesión</a>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
        @csrf
    </form>
</div>
