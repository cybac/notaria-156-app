<div class="app-header white box-shadow">
    <div class="navbar navbar-toggleable-sm flex-row align-items-center">
        <a data-toggle="modal" data-target="#aside" class="hidden-lg-up mr-3">
        <i class="material-icons">&#xe5d2;</i>
        </a>
        <div class="mb-0 h5 no-wrap" ng-bind="$state.current.data.title" id="pageTitle"></div>
        <div class="collapse navbar-collapse" id="collapse">
            <ul class="nav navbar-nav mr-auto"></ul>
        </div>
        <ul class="nav navbar-nav ml-auto flex-row">
            <li class="nav-item dropdown">
                <a class="nav-link p-0 clear" href="#" data-toggle="dropdown">
                    <span class="avatar w-32"><img src="{{asset('images/a0.jpg')}}"><i class="on b-white bottom"></i></span>
                </a>
                @include('includes.perfil')                  
            </li>
            <li class="nav-item dropdown pos-stc-xs">
                <a class="nav-link mr-2" href data-toggle="dropdown">
                <i class="material-icons">&#xe7f5;</i>
                <span class="label label-sm up warn">{{session('recordatorios')->count()}}</span>
                </a>
                @include('includes.notificaciones')
            </li>
            
        </ul>
    </div>
</div>