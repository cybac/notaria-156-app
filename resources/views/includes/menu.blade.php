<div id="aside" class="app-aside modal nav-dropdown">
    <div class="left navside dark dk" data-layout="column">
        <div class="navbar no-radius">
            <a class="navbar-brand" href="{{route('notaria')}}" >
            <div ui-include="'{{asset('images/logo.svg')}}'"></div>
            <img src="{{asset('images/logo.png')}}" alt="." class="hide">
            <span class="hidden-folded inline">Notaria</span>
            </a>
        </div>
        <div class="hide-scroll" data-flex>
            <nav class="scroll nav-light">
                <ul class="nav" ui-nav>
                    <li class="nav-header hidden-folded">
                        <small class="text-muted">Principales</small>
                    </li>
                    <li>
                        <a href="{{route('notaria')}}" >
                        <span class="nav-icon">
                            <i class="material-icons fas fa-home"></i>
                        </span>
                        <span class="nav-text">Inicio</span>
                        </a>
                    </li>
                    <li>
                        <a>
                        <span class="nav-caret">
                        <i class="fa fa-caret-down"></i>
                        </span>
                        <span class="nav-icon">
                        <i class="material-icons fas fa-archive"></i>
                        </span>
                        <span class="nav-text">Expediente</span>
                        </a>
                        <ul class="nav-sub">
                            <li>
                                <a href="{{route('crear_expediente')}}" >
                                    <span class="nav-text">Nuevo</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{route('expedientes')}}" >
                                    <span class="nav-text">Listado</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a>
                        <span class="nav-caret">
                        <i class="fa fa-caret-down"></i>
                        </span>
                        <span class="nav-icon">
                        <i class="material-icons fas fa-address-book">
                        </i>
                        </span>
                        <span class="nav-text">Clientes</span>
                        </a>
                        <ul class="nav-sub">
                            <li>
                                <a href="{{route('crear_cliente')}}" >
                                    <span class="nav-text">Nuevo</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{route('clientes')}}">
                                    <span class="nav-text">Listado</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    @hasrole('administrador')
                    <li>
                        <a>
                        <span class="nav-caret">
                        <i class="fa fa-caret-down"></i>
                        </span>
                        <span class="nav-icon">
                        <i class="material-icons fas fa-user-tie">
                        </i>
                        </span>
                        <span class="nav-text">Empleados</span>
                        </a>
                        <ul class="nav-sub">
                            <li>
                                <a href="{{route('crear_empleado')}}" >
                                    <span class="nav-text">Nuevo</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{route('empleados')}}" >
                                    <span class="nav-text">Listado</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a>
                        <span class="nav-caret">
                        <i class="fa fa-caret-down"></i>
                        </span>
                        <span class="nav-icon">
                        <i class="material-icons fas fa-clipboard">
                        </i>
                        </span>
                        <span class="nav-text">Asuntos</span>
                        </a>
                        <ul class="nav-sub">
                            <li>
                                <a href="{{route('crear_asunto')}}" >
                                    <span class="nav-text">Nuevo</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{route('asuntos')}}" >
                                    <span class="nav-text">Listado</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="{{route('listar_folios')}}" >
                        <span class="nav-icon">
                            <i class="material-icons fas fa-book"></i>
                        </span>
                        <span class="nav-text">Folios</span>
                        </a>
                    </li>
                    @endhasrole
                </ul>
            </nav>
        </div>
        <div class="b-t">
            <div class="nav-fold">
                <span class="pull-left">
                <img src="{{asset('images/a0.jpg')}}" alt="..." class="w-40 img-circle">
                </span>
                <span class="clear hidden-folded p-x">
                <span class="block _500">{{session('nombre')}}</span>
                <small class="block text-muted"><i class="fa fa-circle text-success m-r-sm"></i>activo</small>
                </span>
            </div>
        </div>
    </div>
</div>