<div class="app-footer">
    <div class="p-2 text-xs">
        <div class="pull-right text-muted py-1">
            &copy; COPYRIGHT <strong>NOTARIAS TODOS LOS DERECHOS RESERVADOS 2021</strong> <span class="hidden-xs-down"> </span>
            <a ui-scroll-to="content"><i class="fa fa-long-arrow-up p-x-sm"></i></a>
        </div>
        <div class="nav">
            <a target="_blank" class="nav-link" href="https://www.grupocybac.com">HOSTING Y DISEÑO WEB CYBAC TI.</a>
        </div>
    </div>
</div>