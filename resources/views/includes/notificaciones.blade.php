<div class="dropdown-menu dropdown-menu-overlay pull-right w-xl animated fadeInUp no-bg no-border no-shadow">
    <div class="scrollable" style="max-height: 220px">
        <ul class="list-group list-group-gap m-0">
            @if (session('recordatorios')->count() == 0)
            <li class="list-group-item black lt box-shadow-z0 b">
                <span class="clear block">Sin notificaciones</span>
            </li>
            @else
            @php
            $i = 0;
            setlocale(LC_TIME, 'es_ES.UTF-8');
            @endphp
            @foreach (session('recordatorios') as $item)
            <a href="{{route('recordatorios', $item->expediente)}}">
                @if ($i%2 == 0)
                <li class="list-group-item black lt box-shadow-z0 b">
                    @else
                <li class="list-group-item dark-white text-color box-shadow-z0 b">
                    @endif
                    <span class="clear block">{{$item->titulo}}<br>
                        <small class="text-muted">Creado el {{ strftime("%d de %B, de %Y", strtotime($item->created_at)) }}</small>
                    </span>
                </li>
            </a>
            @php $i++; @endphp
            @endforeach
            @endif
        </ul>
    </div>
</div>
