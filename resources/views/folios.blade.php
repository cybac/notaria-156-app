@extends('layouts.admin')
@section('titulo', "Folios")
@section('content')
<div style="margin: 70px 15px 0px; padding-top: 50px; padding-bottom: 70px;">
    <div class="card w-100 p-5">
        <h3 class="text-center">Folios</h3>
        <div class="w-100 text-right form-group">
            <a class="btn btn-success col-md-2" href="{{ route('crear_folio', $expediente) }}">Registrar</a>
        </div>
        <table class="table" id="dt_folios" data-url="{{ route('dt_folios', $expediente) }}" style="width: 100%">
            <thead>
                <tr>
                    <th>No. Escritura</th>
                    <th>Volumen</th>
                    <th>Folio</th>
                    <th>Actos</th>
                    <th>Partes</th>
                    <th>Notas</th>
                    <th>Fecha</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection