<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmpleadosSeeder extends Seeder
{
    public function run()
    {
        DB::table('empleados')->insert([ 
            [
                'nombre'    =>'Soporte',
                'paterno' => 'Cybac',
                'materno' => '',
                'telefono' => '0000000000'
            ],[
                'nombre'    =>'Ovidio',
                'paterno' => 'Cortazar',
                'materno' => 'Ramos',
                'telefono' => '0000000000'
            ],[
                'nombre'    =>'Empleado',
                'paterno' => 'Test',
                'materno' => '1',
                'telefono' => '9610000001'
            ]
        ]);
    }
}
