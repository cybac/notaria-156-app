<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call([
            TiposDocumentosSeeder::class,
            EmpleadosSeeder::class,
            UsersSeeder::class,
            AsuntosSeeder::class,
        ]);
    }
}
