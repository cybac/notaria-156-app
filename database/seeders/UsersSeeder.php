<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class UsersSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->insert([ 
            [
                'empleado'  =>'1',
                'email'     =>'soporte@grupocybac.com',
                'password'  =>bcrypt('SoporteNotaria_CYBAC')
            ],[
                'empleado'  =>'2',
                'email'     =>'ovidiocortazarramos@hotmail.com',
                'password'  =>bcrypt('NotariaAdministrador1')
            ]
        ]);

        Role::create(['name' => 'administrador']);
        Role::create(['name' => 'notario']);
        Role::create(['name' => 'capturista']);

        User::find(1)->assignRole('administrador');
        User::find(2)->assignRole('administrador');
    }
}
