<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TiposDocumentosSeeder extends Seeder
{
    public function run()
    {
        DB::table('tipos_documentos')->insert([ 
            [
                'nombre'    =>'Generales'
            ],[
                'nombre'    =>'Impuestos y derechos'
            ],[
                'nombre'    =>'Otros'
            ]
        ]);
    }
}
