<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClientesSeeder extends Seeder
{
    public function run()
    {
        DB::table('clientes')->insert([ 
            [
                'nombre'    =>'Luis Fernando',
                'paterno' => 'Suasnavar',
                'materno' => 'Solis',
                'telefono' => '9612295959'
            ],
            [
                'nombre'    =>'Juan',
                'paterno' => 'Perez',
                'materno' => 'Lopez',
                'telefono' => '9665841201'
            ],
            [
                'nombre'    =>'Maria',
                'paterno' => 'Garcia',
                'materno' => 'Zamayoa',
                'telefono' => '9645587425'
            ]
        ]);

    }
}
