<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AsuntosSeeder extends Seeder
{
    public function run()
    {
        DB::table('asuntos')->insert([ 
            [
                'nombre'    =>'Adjudicación',
                'valoracion'=>1
            ],
            [
                'nombre'    =>'Alcance y/o rectificación',
                'valoracion'=>1
            ],
            [
                'nombre'    =>'Cancelación de hipoteca',
                'valoracion'=>1
            ],
            [
                'nombre'    =>'Compraventa',
                'valoracion'=>1
            ],
            [
                'nombre'    =>'Compraventa con la concurrencia de FOVISSSTE',
                'valoracion'=>1
            ],
            [
                'nombre'    =>'Compraventa con la concurrencia de INFONAVIT',
                'valoracion'=>1
            ],
            [
                'nombre'    =>'Constitución de asociaciones',
                'valoracion'=>1
            ],
            [
                'nombre'    =>'Constitución de sociedades',
                'valoracion'=>1
            ],
            [
                'nombre'    =>'Cotejos y copias certificadas',
                'valoracion'=>1
            ],
            [
                'nombre'    =>'Dación de pago',
                'valoracion'=>1
            ],
            [
                'nombre'    =>'Donaciones',
                'valoracion'=>1
            ],
            [
                'nombre'    =>'Fe de hechos',
                'valoracion'=>1
            ],
            [
                'nombre'    =>'Fideicomisos',
                'valoracion'=>1
            ],
            [
                'nombre'    =>'Información testimonial',
                'valoracion'=>1
            ],
            [
                'nombre'    =>'Juicios Sucesorios',
                'valoracion'=>1
            ],
            [
                'nombre'    =>'Lotificación y regimen en condominio',
                'valoracion'=>1
            ],
            [
                'nombre'    =>'Permutas',
                'valoracion'=>1
            ],
            [
                'nombre'    =>'Poderes',
                'valoracion'=>1
            ],
            [
                'nombre'    =>'Protocolización de acta de asamblea',
                'valoracion'=>1
            ],
            [
                'nombre'    =>'Protocolización de contratos privados no traslativos de dominio',
                'valoracion'=>1
            ],
            [
                'nombre'    =>'Ratificación de firma y contenido',
                'valoracion'=>1
            ],
            [
                'nombre'    =>'Reconocimientos de adeudos',
                'valoracion'=>1
            ],
            [
                'nombre'    =>'Revocación de poderes',
                'valoracion'=>1
            ],
            [
                'nombre'    =>'Testamentos',
                'valoracion'=>1
            ]
        ]);
    }
}
