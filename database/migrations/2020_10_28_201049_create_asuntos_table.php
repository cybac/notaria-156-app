<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAsuntosTable extends Migration
{
    public function up()
    {
        Schema::create('asuntos', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->integer('valoracion');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('asuntos');
    }
}
