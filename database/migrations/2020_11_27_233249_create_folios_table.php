<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFoliosTable extends Migration
{
    public function up()
    {
        Schema::create('folios', function (Blueprint $table) {
            $table->id();
            $table->string('escritura');
            $table->string('volumen');
            $table->string('folio_inicio');
            $table->string('folio_fin');
            $table->text('actos');
            $table->text('partes');
            $table->text('notas')->nullable();
            $table->foreignId('empleado');
            $table->foreignId('expediente');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('folios');
    }
}
