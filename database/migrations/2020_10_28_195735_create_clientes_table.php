<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientesTable extends Migration
{
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->id();
            $table->string('razon_social')->default("Física");
            $table->string('nombre');
            $table->string('paterno')->default(" ");
            $table->string('materno')->default(" ");
            $table->string('RFC');
            $table->string('CURP')->default(" ");
            $table->string('telefono')->default(" ");
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('clientes');
    }
}
