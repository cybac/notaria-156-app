<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Expedientes extends Model
{
    protected $table = 'expedientes';

    protected $fillable = ['asunto', 'cliente', 'responsable', 'propio','status'];
}
