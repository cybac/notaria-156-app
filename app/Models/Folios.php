<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Folios extends Model
{
    protected $table = 'folios';

    protected $fillable = ['escritura', 'volumen', 'folio_inicio', 'folio_fin', 'actos', 'partes', 'notas', 'empleado', 'expediente'];
}
