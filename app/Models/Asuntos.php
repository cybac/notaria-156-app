<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Asuntos extends Model
{
    protected $table = 'asuntos';

    protected $fillable = ['nombre', 'valoracion'];
}
