<?php

namespace App\Http\Controllers;

use DataTables;
use App\Models\Pagos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class PagosController extends Controller
{
    public function index($id_cliente)
    {
        return view('pagos',[
            'menu'      =>'clientes',
            'submenu'   =>'',
            'cliente'   =>$id_cliente
        ]);

    }

    public function crear($id_cliente)
    {
        return view('forms.pagos',[
            'form_edit' =>false,
            'menu'      =>'clientes',
            'submenu'   =>'',
            'cliente'   =>$id_cliente
        ]);
    }

    public function datatable(Request $request, $id_cliente)
    {
        $pago = Pagos::where('cliente', '=', $id_cliente);
        return DataTables::of($pago)
        ->addColumn('fecha', function($pago){
            return date_format($pago->created_at, "d/m/Y");
        })
        ->addColumn('btn', function($pago){
            return "<a class='btn btn-success text-white p-3' href='".route('editar_pago',$pago->id)."' style='margin-bottom: 10px;'>Editar</a>";
        })
        ->rawColumns(['fecha' ,'btn'])
        ->toJson();
    }

    public function editar($id_pago)
    {
        $pago = Pagos::find($id_pago);
        return view('forms.pagos',[
            'form_edit' =>true,
            'menu'      =>'clientes',
            'submenu'   =>'',
            'pago'      =>$pago,
            'cliente'   =>$pago->cliente
        ]);
    }

    public function guardar(Request $request)
    {
        $this->validate($request,[
            'cliente'       =>'required',
            'nombre'        =>'required',
            'monto'         =>'required|numeric',
            'descripcion'   =>'required',
        ],
        [
            'cliente.required'      =>'Ingrese el id del cliente',
            'nombre.required'       =>'Ingrese el nombre del pago',
            'monto.required'        =>'Ingrese el monto del pago',
            'descripcion.required'  =>'Ingrese un descripción para el pago',
            'monto.numeric'         =>'Ingrese un valor valido',
        ]);

        $pago = Pagos::create([
            'cliente'       => $request->cliente,
            'nombre'        => $request->nombre,
            'monto'         => $request->monto,
            'descripcion'   => $request->descripcion
        ]);
        return response()->json([
            'status'=>  'true',
            'type'  =>  'success',
            'title' =>  'Éxito',
            'text'  =>  'Se han guardado el pago del cliente',
            'load'  =>  true,
            'url'   =>  route('pagos',$request->cliente)
        ], 200);
    }

    public function actualizar(Request $request, $id_pago)
    {
        $this->validate($request,[
            'cliente'       =>'required',
            'nombre'        =>'required',
            'monto'         =>'required|numeric',
            'descripcion'   =>'required',
        ],
        [
            'cliente.required'      =>'Ingrese el id del cliente',
            'nombre.required'       =>'Ingrese el nombre del pago',
            'monto.required'        =>'Ingrese el monto del pago',
            'descripcion.required'  =>'Ingrese un descripción para el pago',
            'monto.numeric'         =>'Ingrese un valor valido',
        ]);

        $pago = Pagos::find($id_pago);
        $pago->nombre = $request->nombre;
        $pago->monto = $request->monto;
        $pago->descripcion = $request->descripcion;
        $pago->save();
        return response()->json([
            'status'=>  'true',
            'type'  =>  'success',
            'title' =>  'Éxito',
            'text'  =>  'Se ha actualizado los datos del pago',
            'load'  =>  true,
            'url'   =>  route('pagos', $request->cliente)
        ], 200);
    }
}
