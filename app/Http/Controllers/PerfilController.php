<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Empleados;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class PerfilController extends Controller
{
    public function index()
    {
        $perfil = Empleados::where('id', '=', Auth::user()->empleado)->first();
        return view('forms.perfil',[
            'perfil'     => $perfil
        ]);
    }

    public function password()
    {
        return view('forms.password');
    }

    public function actualizar(Request $request)
    {
        $this->validate($request,[
            'nombre'    =>'required',
            'paterno'    =>'required',
            'materno'    =>'required',
            'telefono'    =>'required|numeric',
        ],
        [
            'nombre.required'   =>'Ingrese el nombre del empleado',
            'paterno.required' =>'Ingrese el apellido paterno del empleado',
            'materno.required'   =>'Ingrese el apellido materno del empleadocliente',
            'telefono.required'   =>'Ingrese un numero de contacto para el empleado',
            'telefono.required'   =>'El telefono tiene que ser en formato numerico',
        ]);

        $empleado = Empleados::find(Auth::user()->empleado);
        $empleado->nombre = $request->nombre;
        $empleado->paterno = $request->paterno;
        $empleado->materno = $request->materno;
        $empleado->telefono = $request->telefono;
        $empleado->save();
        return response()->json([
            'status'=>  'true',
            'type'  =>  'success',
            'title' =>  'Éxito',
            'text'  =>  'Se han actualizado tus datos',
            'load'  =>  true,
            'url'   =>  route('notaria')
        ], 200);
    }

    public function actualizar_pass(Request $request)
    {
        $this->validate($request,[
            'password'      =>'required',
            'new_password'  =>'required',
            'conf_password' =>'required',
        ],
        [
            'password.required'         =>'Indique la contraseña actual',
            'new_password.required'     =>'Ingrese la nueva contraseña',
            'conf_password.required'    =>'Confirme la nueva contraseña',
        ]);

        if(!Hash::check($request->password, Auth::user()->password))
        {
            $error['password']='Contraseña incorrecta';
            return response()->json(['errors' => $error], 422);
        }

        if($request->new_password === $request->conf_password)
        {
            $user = User::find(Auth::id());
            $user->password = Hash::make($request->new_password);
            $user->save();
            return response()->json([
                'status'=>  'true',
                'type'  =>  'success',
                'title' =>  'Éxito',
                'text'  =>  'Se han actualizado tu contraseña',
                'load'  =>  true,
                'url'   =>  route('notaria')
            ], 200);

        }else{
            $error['new_password']='Los campos no coincide';
            $error['conf_password']='Los campos no coincide';
            return response()->json(['errors' => $error], 422);

        }
    }
}
