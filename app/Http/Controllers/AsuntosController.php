<?php

namespace App\Http\Controllers;

use DataTables;
use App\Models\Asuntos;
use Illuminate\Http\Request;

class AsuntosController extends Controller
{
    public function index()
    {
        return view('asuntos',[
            'menu'      =>'asuntos',
            'submenu'   =>''
        ]);

    }

    public function datatable(Request $request)
    {
        $asuntos = Asuntos::all();
        return DataTables::of($asuntos)
        ->addColumn('btn', function($asuntos){
            return "<a class='btn btn-success text-white p-3' href='".route('editar_asunto',$asuntos->id)."' style='width: 100%; margin-bottom: 10px;'>Editar</a>";
        })
        ->rawColumns(['btn'])
        ->toJson();
    }

    public function crear()
    {
        return view('forms.asuntos',[
            'form_edit' =>false,
            'menu'      =>'Asuntos',
            'submenu'   =>''
        ]);
    }

    public function guardar(Request $request)
    {
        $this->validate($request,[
            'nombre'        =>'required',
            'valoracion'    =>'required|numeric|min:1|max:100',
        ],
        [
            'nombre.required'           =>'Ingrese el nombre del asunto',
            'valoracion.required'       =>'Ingrese la ponderación del asunto',
            'valoracion.numeric'        =>'Solo se aceptan numeros',
            'valoracion.min'            =>'Ingrese un numero valido, valor mínimo 1',
            'valoracion.max'            =>'Ingrese un numero valido, valor máximo 100',
        ]);

        $asunto = Asuntos::create([
            'nombre' => $request->nombre,
            'valoracion' => $request->valoracion
        ]);

        

        return response()->json([
            'status'=>  'true',
            'type'  =>  'success',
            'title' =>  'Éxito',
            'text'  =>  'Se han creado un nuevo asunto',
            'load'  =>  true,
            'url'   =>  route('asuntos')
        ], 200);
    }

    public function editar($id_asunto)
    {
        $asunto = Asuntos::find($id_asunto);
        return view('forms.asuntos',[
            'form_edit' =>true,
            'menu'      =>'asuntos',
            'submenu'   =>'',
            'asunto'  =>$asunto
        ]);
    }

    public function actualizar(Request $request, $id_asunto)
    {
        $this->validate($request,[
            'nombre'        =>'required',
            'valoracion'    =>'required|numeric|min:1|max:100',
        ],
        [
            'nombre.required'           =>'Ingrese el nombre del asunto',
            'valoracion.required'       =>'Ingrese la ponderación del asunto',
            'valoracion.numeric'        =>'Solo se aceptan numeros',
            'valoracion.min'            =>'Ingrese un numero valido, valor mínimo 1',
            'valoracion.max'            =>'Ingrese un numero valido, valor máximo 100',
        ]);

        $asunto = Asuntos::find($id_asunto);
        $asunto->nombre = $request->nombre;
        $asunto->valoracion = $request->valoracion;
        $asunto->save();
        return response()->json([
            'status'=>  'true',
            'type'  =>  'success',
            'title' =>  'Éxito',
            'text'  =>  'Se ha actualizado los datos del asunto',
            'load'  =>  true,
            'url'   =>  route('asuntos')
        ], 200);
    }
}
