<?php

namespace App\Http\Controllers;

use DataTables;
use App\Models\Expedientes;
use App\Models\Recordatorios;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RecordatoriosController extends Controller
{
    public function index($id_expediente)
    {
        $existe = Expedientes::find($id_expediente);
        if(empty($existe))
        {
            return redirect()->route('expedientes');
        }else{
            if(auth()->user()->hasRole('administrador'))
            {
                return view('recordatorios',[
                    'menu'      =>'expedientes',
                    'submenu'   =>'',
                    'expediente'  =>$id_expediente
                ]);
            }else{
                if(Auth::user()->empleado == $existe->responsable)
                {
                    return view('recordatorios',[
                        'menu'      =>'expedientes',
                        'submenu'   =>'',
                        'expediente'  =>$id_expediente
                    ]);
                }else{
                    return redirect()->route('expedientes');
                }
            }
        }
    }

    public function crear($id_expediente)
    {
        $existe = Expedientes::find($id_expediente);
        if(empty($existe))
        {
            return redirect()->route('expedientes');
        }else{
            if(auth()->user()->hasRole('administrador'))
            {
                return view('forms.recordatorios',[
                    'form_edit' =>false,
                    'menu'      =>'expedientes',
                    'submenu'   =>'',
                    'expediente'=>$id_expediente
                ]);
            }else{
                if(Auth::user()->empleado == $existe->responsable)
                {
                    return view('forms.recordatorios',[
                        'form_edit' =>false,
                        'menu'      =>'expedientes',
                        'submenu'   =>'',
                        'expediente'=>$id_expediente
                    ]);
                }else{
                    return redirect()->route('expedientes');
                }
            }
        }
    }

    public function guardar(Request $request, $id_expediente)
    {
        $this->validate($request,[
            'expediente'    =>'required',
            'titulo'        =>'required',
            'contenido'     =>'required',
            'fecha'         =>'required'
        ],
        [
            'expediente.required'   =>'Ingrese el número del expediente',
            'titulo.required'       =>'Ingrese el número de escritura',
            'contenido.required'    =>'Ingrese el número de volumen',
            'fecha.required'        =>'Ingrese el número de folio de inicio'
        ]);

        $recordatorio = Recordatorios::create([
            'expediente'    => $id_expediente,
            'titulo'        => $request->titulo,
            'contenido'     => $request->contenido,
            'fecha'         => $request->fecha]);

        return response()->json([
            'status'=>  'true',
            'type'  =>  'success',
            'title' =>  'Éxito',
            'text'  =>  'Se han guardado el recordatorio',
            'load'  =>  true,
            'url'   =>  route('recordatorios',$id_expediente)
        ], 200);
    }

    public function datatable(Request $request, $id_expediente)
    {
        $recordatorios = Recordatorios::where('expediente', '=', $id_expediente);
        
        return DataTables::of($recordatorios)
        ->addColumn('fecha', function($recordatorios){
            return date("d/m/Y", strtotime($recordatorios->fecha));
        })
        ->addColumn('status', function($recordatorios){
            if($recordatorios->status)
            {
                return "Pendiente";
            }else{
                return "Visto";
            }
        })
        ->addColumn('btn', function($recordatorios){
            return "<a class='btn btn-primary text-white p-3' href='#' style='width:80%; margin-bottom: 10px;'>Opción</a>";
        })
        ->rawColumns(['fecha', 'status', 'btn'])
        ->toJson();
    }
}
