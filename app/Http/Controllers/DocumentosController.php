<?php

namespace App\Http\Controllers;

use File;
use DataTables;
use App\Models\Documentos;
use App\Models\Expedientes;
use App\Models\Seguimiento;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;


class DocumentosController extends Controller
{
    private $path = "documentos/";
    private $path_generales = "documentos/generales";
    private $path_id = "documentos/impuestos y derechos";
    private $path_otros = "documentos/otros";
    /*
    1.- Para documento generales
    2.- Para documentos de impuestos y derechos
    3.- Para otros documentos
    */

    public function datatable(Request $request, $id_expediente, $criterio)
    {
        switch($criterio)
        {
            case 1:
                $ruta = $this->path_generales;
            break;
            case 2:
                $ruta = $this->path_id;
            break;
            case 3:
                $ruta = $this->path_otros;
            break;
        }
        $documentos = Documentos::select('id', 'alias','tipo', DB::raw('CONCAT("'.asset($ruta).'/",nombre) as archivo'), 'status', DB::raw('DATE_FORMAT(created_at, "%d/%m/%Y") as fecha'))
            ->where('expediente', '=', $id_expediente)
            ->where('tipo', '=', $criterio);
        return DataTables::of($documentos)
        ->addColumn('btn', function($documentos){
            if($documentos->status)
            {
                return "<button class='btn btn-primary ver-archivo text-white p-3' data-tipo='".substr($documentos->archivo, Str::length($documentos->archivo) - 3, 3)."' data-archivo='".$documentos->archivo."' style='width:30%; margin-bottom: 10px; cursor: pointer;'>Mostrar</button>";
            }else{
                return "<a class='btn btn-success text-white p-3' href='".route('editar_general',$documentos->id)."' style='width:30%; margin-bottom: 10px;'>Subir</a>";
            }
        })
        ->addColumn('status', function($documentos){
            if($documentos->status)
            {
                return "Archivo subido";
            }else{
                return "Pendiente";
            }
        })
        ->rawColumns(['status','btn'])
        ->toJson();
    }

    public function documentos_generales($id_expediente)
    {
        $existe = Expedientes::find($id_expediente);
        if(empty($existe))
        {
            //NO existe el expediente
            return redirect()->route('expedientes');
        }else{
            //Existe el expediente
            if(auth()->user()->hasRole('administrador'))
            {
                return view('documentos',[
                    'menu'          =>'expedientes',
                    'submenu'       =>'',
                    'expediente'    =>$id_expediente,
                    'criterio'      =>'1'
                ]);
            }else{
                if(Auth::user()->empleado == $existe->responsable)
                {
                    //el responsable entro
                    return view('documentos',[
                        'menu'          =>'expedientes',
                        'submenu'       =>'',
                        'expediente'    =>$id_expediente,
                        'criterio'      =>'1'
                    ]);
                }else{
                    //es otro usuario, sacalo de aqui
                    return redirect()->route('expedientes');
                }
            }
        }
    }

    public function documentos_id($id_expediente)
    {
        $existe = Expedientes::find($id_expediente);
        if(empty($existe))
        {
            //No existe el expediente
            return redirect()->route('expedientes');
        }else{
            if(auth()->user()->hasRole('administrador'))
            {
                return view('documentos',[
                    'menu'          =>'expedientes',
                    'submenu'       =>'',
                    'expediente'    =>$id_expediente,
                    'criterio'      =>'2'
                ]);
            }else{
                if(Auth::user()->empleado == $existe->responsable)
                {
                    return view('documentos',[
                        'menu'          =>'expedientes',
                        'submenu'       =>'',
                        'expediente'    =>$id_expediente,
                        'criterio'      =>'2'
                    ]);
                }else{
                    return redirect()->route('expedientes');
                }
            }
        }
    }

    public function documentos_otros($id_expediente)
    {
        $existe = Expedientes::find($id_expediente);
        if(empty($existe))
        {
            //No existe el expediente
            return redirect()->route('expedientes');
        }else{
            if(auth()->user()->hasRole('administrador'))
            {
                return view('documentos',[
                    'menu'          =>'expedientes',
                    'submenu'       =>'',
                    'expediente'    =>$id_expediente,
                    'criterio'      =>'3'
                ]);
            }else{
                if(Auth::user()->empleado == $existe->responsable){
                    return view('documentos',[
                        'menu'          =>'expedientes',
                        'submenu'       =>'',
                        'expediente'    =>$id_expediente,
                        'criterio'      =>'3'
                    ]); 
                }else{
                    return redirect()->route('expedientes');
                }
            }
        }
    }

    public function subir_id($id_expediente)
    {
        $existe = Expedientes::find($id_expediente);
        if(empty($existe))
        {
            //No existe el expediente
            return redirect()->route('expedientes');
        }else{
            if(auth()->user()->hasRole('administrador'))
            {
                return view('forms.documentos.subir_id',[
                    'form_edit' =>false,
                    'menu'      =>'expedientes',
                    'submenu'   =>'',
                    'expediente'      =>$id_expediente
                ]);
            }else{
                if(Auth::user()->empleado == $existe->responsable)
                {
                    return view('forms.documentos.subir_id',[
                        'form_edit' =>false,
                        'menu'      =>'expedientes',
                        'submenu'   =>'',
                        'expediente'      =>$id_expediente
                    ]);
                }else{
                    return redirect()->route('expedientes');
                }
            }
        }
    }

    public function subir_otro($id_expediente)
    {
        $existe = Expedientes::find($id_expediente);
        if(empty($existe))
        {
            //No existe el expediente
            return redirect()->route('expedientes');
        }else{
            if(auth()->user()->hasRole('administrador'))
            {
                return view('forms.documentos.subir_otro',[
                    'form_edit' =>false,
                    'menu'      =>'expedientes',
                    'submenu'   =>'',
                    'expediente'      =>$id_expediente
                ]);
            }else{
                if(Auth::user()->empleado == $existe->responsable)
                {
                    return view('forms.documentos.subir_otro',[
                        'form_edit' =>false,
                        'menu'      =>'expedientes',
                        'submenu'   =>'',
                        'expediente'      =>$id_expediente
                    ]);
                }else{
                    return redirect()->route('expedientes');
                }
            }
        }
    }

    public function guardar(Request $request, $tipo)
    {
        $rules = [
            'expediente'    =>'required',
            'alias'         =>'required|string',
            'documento'     =>'required|mimes:pdf,png,jpg,jpeg|max:10240'
        ];
        
        $messages = [
            'expediente.required'       =>'Indique el expediente al que se subira el documento',
            'alias.required'            =>'Indique el tipo de documento a subir',
            'documento.required'        =>'Indique el documento a subir',
            'documento.mimes'           =>'Solo se permiten imágenes PNG, JPG, JPEG o archivos PDF',
            'documento.max'             =>'El tamaño maximo del archivo es de 10MB'
        ];
        $this->validate($request, $rules, $messages);
        
        if($request->hasFile('documento')){
            $archivo = $request->file('documento');
            if (!file_exists($this->path)) {
                File::makeDirectory($this->path, $mode = 0777, true, true);
            }
            if (!file_exists($this->path_generales)) {
                File::makeDirectory($this->path_generales, $mode = 0777, true, true);
            }
            if (!file_exists($this->path_id)) {
                File::makeDirectory($this->path_id, $mode = 0777, true, true);
            }
            if (!file_exists($this->path_otros)) {
                File::makeDirectory($this->path_otros, $mode = 0777, true, true);
            }
            $file_name = uniqid().".".$archivo->extension();    //Nombre del documento
            switch($tipo)
            {
                case 1:
                    $archivo->move($this->path_generales, $file_name);
                    $seguimiento = Seguimiento::create([
                        'expediente'    =>$request->expediente,
                        'titulo'        =>'Subida de documento',
                        'descripcion'   =>'Se subió un documento general llamado: '.$request->alias.' que estaba pendiente de ser subido'
                    ]);
                break;
                case 2:
                    $archivo->move($this->path_id, $file_name);
                    $seguimiento = Seguimiento::create([
                        'expediente'    =>$request->expediente,
                        'titulo'        =>'Subida de documento',
                        'descripcion'   =>'Se subió un documento de impuestos y derechos llamado: '.$request->alias
                    ]);
                break;
                case 3:
                    $archivo->move($this->path_otros, $file_name);
                    $seguimiento = Seguimiento::create([
                        'expediente'    =>$request->expediente,
                        'titulo'        =>'Subida de documento',
                        'descripcion'   =>'Se subió un documento llamado: '.$request->alias
                    ]);
                break;
            }
            $documento = Documentos::create([
                'expediente'    =>$request->expediente,
                'tipo'          =>$tipo,
                'alias'         =>$request->alias,
                'nombre'        =>$file_name,
                'status'        =>true
            ]);
        }
        if($tipo == 1)
        {
            return response()->json([
                'status'=>true,
                'type'  =>'success',
                'title' =>'Éxito',
                'text'  =>'Se ha guardado el documento',
                'load'  =>true,
                'url'   =>route('generales', $request->expediente)
            ], 200);
        }else if($tipo == 2)
        {
            return response()->json([
                'status'=>true,
                'type'  =>'success',
                'title' =>'Éxito',
                'text'  =>'Se ha guardado el documento',
                'load'  =>true,
                'url'   =>route('impuestos_derechos', $request->expediente)
            ], 200);
        }else{
            return response()->json([
                'status'=>true,
                'type'  =>'success',
                'title' =>'Éxito',
                'text'  =>'Se ha guardado el documento',
                'load'  =>true,
                'url'   =>route('otros', $request->expediente)
            ], 200);
        }
    }

    public function editar($id_documento)
    {
        $documento = Documentos::find($id_documento);
        if(empty($documento))
        {
            //El documento no existe
            return redirect()->route('expedientes');
        }else{
            //El documento existe
            $responsable = Expedientes::find($documento->expediente);
            if(auth()->user()->hasRole('administrador'))
            {
                if($documento->status)
                {
                    //Cuando no esta pendiente el documento de subir
                    return route('generales', $documento->expediente);
                }else{
                    return view('forms.documentos.subir_general',[
                        'form_edit' =>true,
                        'menu'      =>'documentos',
                        'submenu'   =>'',
                        'documento' =>$documento,
                    ]);
                }
            }else{
                if(Auth::user()->empleado == $responsable->responsable)
                {
                    if($documento->status)
                    {
                        //Cuando no esta pendiente el documento de subir
                        return redirect()->route('generales', $documento->expediente);
                    }else{
                        return view('forms.documentos.subir_general',[
                            'form_edit' =>true,
                            'menu'      =>'documentos',
                            'submenu'   =>'',
                            'documento' =>$documento,
                        ]);
                    }
                }else{
                    return redirect()->route('expedientes');
                }
            }
        }
    }

    public function actualizar(Request $request, $id_documento)
    {
        $rules = [
            'documento'=>'required|mimes:pdf,png,jpg,jpeg|max:10240'
        ];
        
        $messages = [
            'documento.required'        =>'Indique el documento a subir',
            'documento.mimes'           =>'Solo se permiten imágenes PNG, JPG, JPEG o archivos PDF',
            'documento.max'             =>'El tamaño maximo del archivo es de 10MB'
        ];
        $this->validate($request, $rules, $messages);
        
        $documento = Documentos::find($id_documento);
        if($request->hasFile('documento')){
            $archivo = $request->file('documento');
            if (!file_exists($this->path)) {
                File::makeDirectory($this->path, $mode = 0777, true, true);
            }
            if (!file_exists($this->path_generales)) {
                File::makeDirectory($this->path_generales, $mode = 0777, true, true);
            }
            $file_name = uniqid().".".$archivo->extension();        //Nombre del documento
            $archivo->move($this->path_generales, $file_name);      //Se mueve el archivo a la carpeta del contenido digital
            $documento->nombre =$file_name;
            $documento->status = true;
            $documento->save();

            $seguimiento = Seguimiento::create([
                'expediente'    =>$documento->expediente,
                'titulo'        =>'Subida de documento',
                'descripcion'   =>'Se subió un documento general llamado: '.$documento->alias.' que estaba pendiente de ser subido'
            ]);
        }

        return response()->json([
            'status'=>  'true',
            'type'  =>  'success',
            'title' =>  'Éxito',
            'text'  =>  'Se ha subido el archivo correctamente',
            'load'  =>  true,
            'url'   =>  route('generales', $documento->expediente)
        ], 200);
    }
}
