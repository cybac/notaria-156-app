<?php

namespace App\Http\Controllers;

use DataTables;
use App\Models\Folios;
use App\Models\Empleados;
use App\Models\Expedientes;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;


class FoliosController extends Controller
{
    public function index($id_expediente)
    {
        $existe = Expedientes::find($id_expediente);
        if(empty($existe))
        {
            return redirect()->route('expedientes');
        }else{
            if(auth()->user()->hasRole('administrador'))
            {
                return view('folios',[
                    'menu'      =>'expedientes',
                    'submenu'   =>'',
                    'expediente'  =>$id_expediente
                ]);
            }else{
                if(Auth::user()->empleado == $existe->responsable)
                {
                    return view('folios',[
                        'menu'      =>'expedientes',
                        'submenu'   =>'',
                        'expediente'  =>$id_expediente
                    ]);
                }else{
                    return redirect()->route('expedientes');
                }
            }
        }
    }

    public function all()
    {
        return view('list_folios',[
            'menu'      =>'expedientes',
            'submenu'   =>'',
            'all'       =>true
        ]);
    }

    public function folios_empleado($id_empleado)
    {
        $existe = Empleados::find($id_empleado);
        if(empty($existe))
        {
            return redirect()->route('empleados');
        }else{
            return view('list_folios',[
                'menu'      =>'expedientes',
                'submenu'   =>'',
                'all'       =>false,
                'empleado'  =>$id_empleado
            ]);
        }
    }


    public function crear($id_expediente)
    {
        $existe = Expedientes::find($id_expediente);
        if(empty($existe))
        {
            return redirect()->route('expedientes');
        }else{
            if(auth()->user()->hasRole('administrador'))
            {
                return view('forms.folios',[
                    'form_edit' =>false,
                    'menu'      =>'expedientes',
                    'submenu'   =>'',
                    'expediente'=>$id_expediente
                ]);
            }else{
                if(Auth::user()->empleado == $existe->responsable)
                {
                    return view('forms.folios',[
                        'form_edit' =>false,
                        'menu'      =>'expedientes',
                        'submenu'   =>'',
                        'expediente'=>$id_expediente
                    ]);
                }else{
                    return redirect()->route('expedientes');
                }
            }
        }
    }

    public function guardar(Request $request, $id_expediente)
    {
        $this->validate($request,[
            'escritura' =>'required',
            'volumen'   =>'required',
            'folio_ini' =>'required',
            'folio_fin' =>'required',
            'actos'     =>'required',
            'partes'    =>'required|string'
        ],
        [
            'escritura.required'    =>'Ingrese el número de escritura',
            'volumen.required'      =>'Ingrese el número de volumen',
            'folio_ini.required'    =>'Ingrese el número de folio de inicio',
            'folio_fin.required'    =>'Ingrese el número de folio de fin',
            'actos.required'        =>'Ingrese el acto',
            'partes.required'       =>'Indique las partes involucradas'
        ]);

        $folio = Folios::create([
            'escritura' => $request->escritura,
            'volumen'=> $request->volumen,
            'folio_inicio'=> $request->folio_ini,
            'folio_fin'=> $request->folio_fin,
            'actos'=> $request->actos,
            'partes'=> $request->partes,
            'notas' => $request->notas,
            'empleado'=>Auth::user()->empleado,
            'expediente'=>$id_expediente]);

        return response()->json([
            'status'=>  'true',
            'type'  =>  'success',
            'title' =>  'Éxito',
            'text'  =>  'Se han guardado los datos',
            'load'  =>  true,
            'url'   =>  route('expedientes')
        ], 200);
    }

    public function datatable(Request $request, $id_expediente)
    {
        $folios = Folios::where('expediente', '=', $id_expediente);
        
        return DataTables::of($folios)
        ->addColumn('folio', function($folios){
            return $folios->folio_inicio." - ".$folios->folio_fin;
        })
        ->addColumn('fecha', function($folios){
            return date_format($folios->created_at, "d/m/Y");
        })
        ->rawColumns(['folio', 'fecha'])
        ->toJson();
    }

    public function datatable_folios(Request $request)
    {
        $folios = Folios::select('folios.id', 'folios.escritura', 'folios.volumen', DB::raw('CONCAT(folios.folio_inicio, " - ", folios.folio_fin) as folio'), 'folios.actos', 'folios.partes', 'folios.notas', 'folios.expediente', DB::raw('CONCAT(empleados.paterno, " ", empleados.materno, " ", empleados.nombre) as empleado'), 'folios.created_at')
            ->join('empleados','empleados.id', '=', 'folios.empleado');
        
        return DataTables::of($folios)
        ->addColumn('fecha', function($folios){
            return date_format($folios->created_at, "d/m/Y");
        })
        ->rawColumns(['fecha'])
        ->toJson();
    }

    public function datatable_folios_empleado(Request $request, $id_empleado)
    {
        $folios = Folios::select('folios.id', 'folios.escritura', 'folios.volumen', DB::raw('CONCAT(folios.folio_inicio, " - ", folios.folio_fin) as folio'), 'folios.actos', 'folios.partes', 'folios.notas', 'folios.expediente', 'folios.created_at')
            ->join('empleados','empleados.id', '=', 'folios.empleado')
            ->where('folios.empleado', '=', $id_empleado);
        
        return DataTables::of($folios)
        ->addColumn('fecha', function($folios){
            return date_format($folios->created_at, "d/m/Y");
        })
        ->rawColumns(['fecha'])
        ->toJson();
    }
}
