<?php

namespace App\Http\Controllers;

use File;
use Config;
use DataTables;
use App\Models\User;
use App\Models\Asuntos;
use App\Models\Clientes;
use App\Models\Empleados;
use App\Models\Documentos;
use App\Models\Expedientes;
use App\Models\Seguimiento;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ExpedientesController extends Controller
{
    private $path = "documentos/generales";

    public function index()
    {
        $recordatorios = "Hola";
        return view('expedientes',[
            'menu'          =>'expedientes',
            'submenu'       =>'',
            'recordatorios' =>$recordatorios
        ]);
    }

    public function datatable(Request $request)
    {
        if(auth()->user()->hasRole('administrador'))
        {
            //admin
            $expediente = Expedientes::select('expedientes.id','expedientes.propio', 'asuntos.nombre as asunto', DB::raw('concat(clientes.nombre, " ", clientes.paterno, " ", clientes.materno) as cliente'), DB::raw('concat(empleados.nombre, " ", empleados.paterno, " ", empleados.materno) as responsable'), 'expedientes.status', DB::raw('DATE_FORMAT(expedientes.created_at, "%d/%m/%Y") as fecha'))
                ->join('asuntos', 'asuntos.id','=', 'expedientes.asunto')
                ->join('clientes', 'clientes.id','=', 'expedientes.cliente')
                ->join('empleados', 'empleados.id','=', 'expedientes.responsable');
        }else if(auth()->user()->hasRole('capturista')){
            //capturista
            $expediente = Expedientes::select('expedientes.id','expedientes.propio', 'asuntos.nombre as asunto', DB::raw('concat(clientes.nombre, " ", clientes.paterno, " ", clientes.materno) as cliente'), DB::raw('concat(empleados.nombre, " ", empleados.paterno, " ", empleados.materno) as responsable'), 'expedientes.status', DB::raw('DATE_FORMAT(expedientes.created_at, "%d/%m/%Y") as fecha'))
                ->join('asuntos', 'asuntos.id','=', 'expedientes.asunto')
                ->join('clientes', 'clientes.id','=', 'expedientes.cliente')
                ->join('empleados', 'empleados.id','=', 'expedientes.responsable');
        }else{
            //empleados
            $expediente = Expedientes::select('expedientes.id','expedientes.propio', 'asuntos.nombre as asunto', DB::raw('concat(clientes.nombre, " ", clientes.paterno, " ", clientes.materno) as cliente'), DB::raw('concat(empleados.nombre, " ", empleados.paterno, " ", empleados.materno) as responsable'), 'expedientes.status', DB::raw('DATE_FORMAT(expedientes.created_at, "%d/%m/%Y") as fecha'))
                ->join('asuntos', 'asuntos.id','=', 'expedientes.asunto')
                ->join('clientes', 'clientes.id','=', 'expedientes.cliente')
                ->join('empleados', 'empleados.id','=', 'expedientes.responsable')
                ->where('responsable', '=', Auth::user()->empleado);
        }
        return DataTables::of($expediente)
        ->addColumn('fecha', function($expediente){
            return $expediente->fecha;
        })
        ->addColumn('status', function($expediente){
            if($expediente->status)
            {
                return "En proceso";
            }else{
                return "Finalizado";
            }
        })
        ->addColumn('propio', function($expediente){
            if($expediente->propio)
            {
                return "Externo";
            }else{
                return "Notaria";
            }
        })
        ->addColumn('btn', function($expediente){
            return "<a class='btn btn-primary text-white p-3 botones-exp' href='".route('generales',$expediente->id)."'>Generales</a><br>
                <a class='btn btn-warning text-white p-3 botones-exp' href='".route('impuestos_derechos',$expediente->id)."'>Impuesto y derechos</a><br>
                <a class='btn btn-danger text-white p-3 botones-exp' href='".route('otros',$expediente->id)."'>Otros</a>";
        })
        ->addColumn('next', function($expediente){
            return "<a class='btn btn-info text-white p-3 botones-exp' href='".route('seguimiento',$expediente->id)."'>Seguimiento</a><br>
            <a class='btn btn-warning text-white p-3 botones-exp' href='".route('folios',$expediente->id)."'>Folios</a><br>
            <a class='btn btn-success text-white p-3 botones-exp' href='".route('recordatorios',$expediente->id)."'>Recordatorios</a>";
        })
        ->rawColumns(['fecha','btn', 'next'])
        ->toJson();
    }

    public function crear()
    {
        $asuntos = Asuntos::all();
        $clientes = Clientes::where("razon_social", "=", "Física")->get();
        if(auth()->user()->hasRole('administrador'))
        {
            $empleados = Empleados::where('id','>','2')->get();
        }else if(auth()->user()->hasRole('capturista')){
            $empleados = Empleados::where('id','>','2')->where('id', '!=', Auth::user()->empleado)->get();
        }
        else{
            $empleados = Empleados::where('id', '=', Auth::user()->empleado)->get();
        }
        return view('forms.expedientes',[
            'form_edit' =>false,
            'menu'      =>'expedientes',
            'submenu'   =>'',
            'clientes'  =>$clientes,
            'asuntos'   =>$asuntos,
            'empleados' =>$empleados
        ]);
    }

    public function guardar(Request $request)
    {
        $rules = [
            'cliente'       =>'required',
            'asunto'        =>'required',
            'responsable'   =>'required'
        ];
        if($request->hasFile('INE')){
            $rules['INE'] = 'required|mimes:pdf,png,jpg,jpeg|max:10240';
        }
        if($request->hasFile('CURP')){
            $rules['CURP'] = 'required|mimes:pdf,png,jpg,jpeg|max:10240';
        }
        if($request->hasFile('Nacimiento')){
            $rules['Nacimiento'] = 'required|mimes:pdf,png,jpg,jpeg|max:10240';
        }
        if($request->hasFile('CD')){
            $rules['CD'] = 'required|mimes:pdf,png,jpg,jpeg|max:10240';
        }
        if($request->hasFile('RFC'))
        {
            $rules['RFC'] = 'required|mimes:pdf,png,jpg,jpeg|max:10240';
        }
        $messages = [
            'cliente.required'          =>'Seleccione el cliente',
            'asunto.required'           =>'Seleccione el asunto a tratar',
            'responsable.required'      =>'Seleccione quien llevará el expediente',
            'INE.required'              =>'Se requiere el INE',
            'INE.mimes'                 =>'Solo se permiten imágenes PNG, JPG o JPEG o archivos pdf',
            'INE.max'                   =>'El tamaño maximo del archivo es de 10MB',
            'CURP.required'             =>'Se requiere el CURP',
            'CURP.mimes'                =>'Solo se permiten imágenes PNG, JPG o JPEG o archivos pdf',
            'CURP.max'                  =>'El tamaño maximo del archivo es de 10MB',
            'Nacimiento.required'       =>'Se requiere el acta de nacimiento',
            'Nacimiento.mimes'          =>'Solo se permiten imágenes PNG, JPG o JPEG o archivos pdf',
            'Nacimiento.max'            =>'El tamaño maximo del archivo es de 10MB',
            'CD.required'               =>'Se requiere un comprobante de domicilio',
            'CD.mimes'                  =>'Solo se permiten imágenes PNG, JPG o JPEG o archivos pdf',
            'CD.max'                    =>'El tamaño maximo del archivo es de 10MB',
            'RFC.required'              =>'Se requiere el RFC',
            'RFC.mimes'                 =>'Solo se permiten imágenes PNG, JPG o JPEG o archivos pdf',
            'RFC.max'                   =>'El tamaño maximo del archivo es de 10MB'
        ];
        $this->validate($request, $rules, $messages);
        if($request->propio)
        {
            $expediente = Expedientes::create([
                'asunto'        =>$request->asunto,
                'cliente'       =>$request->cliente,
                'responsable'   =>$request->responsable,
                'propio'        =>true,
                'status'        =>true
            ]);
        }else{
            $expediente = Expedientes::create([
                'asunto'        =>$request->asunto,
                'cliente'       =>$request->cliente,
                'responsable'   =>$request->responsable,
                'propio'        =>false,
                'status'        =>true
            ]);
        }

        $seguimiento = Seguimiento::create([
            'expediente'    =>$expediente->id,
            'titulo'        =>'Creacion del expediente',
            'descripcion'   =>'Se dio apertura a un nuevo expediente'
        ]);

        if($request->hasFile('INE')){
            $archivo = $request->file('INE');
            if (!file_exists($this->path)) {
                File::makeDirectory($this->path, $mode = 0777, true, true);
            }
            $file_name = uniqid().".".$archivo->extension();    //Nombre del documento
            $archivo->move($this->path, $file_name);            //Se mueve el archivo a la carpeta del contenido digital
            $documento = Documentos::create([
                'expediente'    =>$expediente->id,
                'tipo'          =>"1",
                'alias'         =>"INE",
                'nombre'        =>$file_name,
                'status'        =>true
            ]);
            $seguimiento = Seguimiento::create([
                'expediente'    =>$expediente->id,
                'titulo'        =>'Subida de documento',
                'descripcion'   =>'Se recibio: el INE al crear el expediente'
            ]);
        }else{
            $documento = Documentos::create([
                'expediente'    =>$expediente->id,
                'tipo'          =>"1",
                'alias'          =>"INE",
                'nombre'        =>"",
                'status'        =>false
            ]);
        }

        if($request->hasFile('CURP')){
            $archivo = $request->file('CURP');
            if (!file_exists($this->path)) {
                File::makeDirectory($this->path, $mode = 0777, true, true);
            }
            $file_name = uniqid().".".$archivo->extension();    //Nombre del documento
            $archivo->move($this->path, $file_name);            //Se mueve el archivo a la carpeta del contenido digital
            $documento = Documentos::create([
                'expediente'    =>$expediente->id,
                'tipo'          =>"1",
                'alias'         =>"CURP",
                'nombre'        =>$file_name,
                'status'        =>true
            ]);
            $seguimiento = Seguimiento::create([
                'expediente'    =>$expediente->id,
                'titulo'        =>'Subida de documento',
                'descripcion'   =>'Se recibio: el CURP al crear el expediente'
            ]);
        }else{
            $documento = Documentos::create([
                'expediente'    =>$expediente->id,
                'tipo'          =>"1",
                'alias'          =>"CURP",
                'nombre'        =>"",
                'status'        =>false
            ]);
        }

        if($request->hasFile('Nacimiento')){
            $archivo = $request->file('Nacimiento');
            if (!file_exists($this->path)) {
                File::makeDirectory($this->path, $mode = 0777, true, true);
            }
            $file_name = uniqid().".".$archivo->extension();    //Nombre del documento
            $archivo->move($this->path, $file_name);            //Se mueve el archivo a la carpeta del contenido digital
            $documento = Documentos::create([
                'expediente'    =>$expediente->id,
                'tipo'          =>"1",
                'alias'         =>"Acta de nacimiento",
                'nombre'        =>$file_name,
                'status'        =>true
            ]);
            $seguimiento = Seguimiento::create([
                'expediente'    =>$expediente->id,
                'titulo'        =>'Subida de documento',
                'descripcion'   =>'Se recibio: el Acta de nacimiento al crear el expediente'
            ]);
        }else{
            $documento = Documentos::create([
                'expediente'    =>$expediente->id,
                'tipo'          =>"1",
                'alias'          =>"Acta de nacimiento",
                'nombre'        =>"",
                'status'        =>false
            ]);
        }

        if($request->hasFile('CD')){
            $archivo = $request->file('CD');
            if (!file_exists($this->path)) {
                File::makeDirectory($this->path, $mode = 0777, true, true);
            }
            $file_name = uniqid().".".$archivo->extension();    //Nombre del documento
            $archivo->move($this->path, $file_name);            //Se mueve el archivo a la carpeta del contenido digital
            $documento = Documentos::create([
                'expediente'    =>$expediente->id,
                'tipo'          =>"1",
                'alias'         =>"Comprobante de domicilio",
                'nombre'        =>$file_name,
                'status'        =>true
            ]);
            $seguimiento = Seguimiento::create([
                'expediente'    =>$expediente->id,
                'titulo'        =>'Subida de documento',
                'descripcion'   =>'Se recibio: un comprobante de domicilio al crear el expediente'
            ]);
        }else{
            $documento = Documentos::create([
                'expediente'    =>$expediente->id,
                'tipo'          =>"1",
                'alias'         =>"Comprobante de domicilio",
                'nombre'        =>"",
                'status'        =>false
            ]);
        }

        if($request->hasFile('RFC')){
            $archivo = $request->file('RFC');
            if (!file_exists($this->path)) {
                File::makeDirectory($this->path, $mode = 0777, true, true);
            }
            $file_name = uniqid().".".$archivo->extension();    //Nombre del documento
            $archivo->move($this->path, $file_name);            //Se mueve el archivo a la carpeta del contenido digital
            $documento = Documentos::create([
                'expediente'    =>$expediente->id,
                'tipo'          =>"1",
                'alias'         =>"RFC",
                'nombre'        =>$file_name,
                'status'        =>true
            ]);
            $seguimiento = Seguimiento::create([
                'expediente'    =>$expediente->id,
                'titulo'        =>'Subida de documento',
                'descripcion'   =>'Se recibio: el RFC al crear el expediente'
            ]);
        }else{
            $documento = Documentos::create([
                'expediente'    =>$expediente->id,
                'tipo'          =>"1",
                'alias'          =>"RFC",
                'nombre'        =>"",
                'status'        =>false
            ]);
        }

        return response()->json([
            'status'=>true,
            'type'  =>'success',
            'title' =>'Éxito',
            'text'  =>'Se ha creado el expediente',
            'load'  =>true,
            'url'   =>route('expedientes')
        ], 200);
    }
}
