<?php

namespace App\Http\Controllers;

use DataTables;
use App\Models\Seguimiento;
use App\Models\Expedientes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class SeguimientoController extends Controller
{
    public function index($id_expediente)
    {
        $existe = Expedientes::find($id_expediente);
        if(empty($existe))
        {
            return redirect()->route('expedientes');
        }else{
            if(auth()->user()->hasRole('administrador'))
            {
                return view('seguimiento',[
                    'menu'          =>'expedientes',
                    'submenu'       =>'',
                    'expediente'    =>$id_expediente
                ]);
            }else{
                if(Auth::user()->empleado == $existe->responsable)
                {
                    return view('seguimiento',[
                        'menu'          =>'expedientes',
                        'submenu'       =>'',
                        'expediente'    =>$id_expediente
                    ]);
                }else{
                    return redirect()->route('expedientes');
                }
            }
        }
    }

    public function datatable(Request $request, $id_expediente)
    {
        $seguimiento = Seguimiento::select('titulo', 'descripcion', DB::raw('DATE_FORMAT(created_at, "%d/%m/%Y") as fecha'))
            ->where('expediente', '=', $id_expediente)
            ->orderByDesc('id');
        return DataTables::of($seguimiento)->toJson();
    }

    public function crear($id_expediente)
    {
        $existe = Expedientes::find($id_expediente);
        if(empty($existe))
        {
            return redirect()->route('expedientes');
        }else{
            if(auth()->user()->hasRole('administrador'))
            {
                return view('forms.seguimiento',[
                    'form_edit' =>false,
                    'menu'      =>'expedientes',
                    'submenu'   =>'',
                    'expediente'=>$id_expediente
                ]);
            }else{
                if(Auth::user()->empleado == $existe->responsable)
                {
                    return view('forms.seguimiento',[
                        'form_edit' =>false,
                        'menu'      =>'expedientes',
                        'submenu'   =>'',
                        'expediente'=>$id_expediente
                    ]);
                }else{
                    return redirect()->route('expedientes');
                }
            }
        }
    }

    public function guardar(Request $request, $id_expediente)
    {
        $this->validate($request,[
            'titulo'        =>'required',
            'descripcion'   =>'required'
        ],
        [
            'titulo.required'       =>'Indique en pocas palabras el movimiento que se realizo',
            'descripcion.required'  =>'Detalles el movimiento que se realizo'
        ]);
        
        $seguimiento = Seguimiento::create([
            'expediente' =>$id_expediente,
            'titulo' => $request->titulo,
            'descripcion' => $request->descripcion
        ]);
        return response()->json([
            'status'=>  'true',
            'type'  =>  'success',
            'title' =>  'Éxito',
            'text'  =>  'Se registrado el movimiento',
            'load'  =>  true,
            'url'   =>  route('seguimiento', $id_expediente)
        ], 200);
    }
}
