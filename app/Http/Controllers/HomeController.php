<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Empleados;
use App\Models\Expedientes;
use App\Models\Recordatorios;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $temp = Empleados::where('id', '=', Auth::user()->empleado)->first();
        $nombre = $temp->nombre." ".$temp->paterno." ".$temp->materno;
        session(['nombre' => $nombre]);

        Auth::user()->empleado;
        $expedientes = Expedientes::select('id')->where('responsable', '=', Auth::user()->empleado)->get();
        $recordatorios = Recordatorios::whereIn('expediente',$expedientes)->get();
        session(['recordatorios' => $recordatorios]);
        return view('notaria');
    }

    public function welcome()
    {
        return redirect(route('login'));
    }
}
