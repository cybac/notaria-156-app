<?php

namespace App\Http\Controllers;

use DataTables;
use App\Models\User;
use App\Models\Empleados;
use App\Models\Expedientes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class EmpleadosController extends Controller
{
    public function index()
    {
        return view('empleados',[
            'menu'      =>'empleados',
            'submenu'   =>''
        ]);
    }

    public function datatable(Request $request)
    {
        $empleado = Empleados::select('empleados.id as e_id', 'users.id as u_id','empleados.nombre', 'empleados.paterno', 'empleados.materno', 'users.email',
            DB::raw('(SELECT count(*) FROM expedientes where responsable = empleados.id) as carga'), 
            DB::raw('(SELECT (SUM(asuntos.valoracion)/count(asuntos.valoracion)) FROM expedientes join asuntos on asuntos.id = expedientes.asunto where expedientes.responsable=empleados.id) as valoracion'))
            ->join('users', 'empleados.id', '=', 'users.empleado')
            ->where('empleados.id', '>', '2')
            ->get();
        return DataTables::of($empleado)
        ->addColumn('nombre', function($empleado){
            return $empleado->paterno.' '.$empleado->materno.' '.$empleado->nombre;
        })
        ->addColumn('btn', function($empleado){
            return "<a class='btn btn-success text-white p-3 botones-exp' href='".route('editar_empleado',$empleado->e_id)."'>Editar</a><br>
            <a class='btn btn-primary text-white p-3 botones-exp' href='".route('expedientes_empleado',$empleado->e_id)."'>Expedientes</a><br>
            <a class='btn btn-warning text-white p-3 botones-exp' href='".route('folios_empleado',$empleado->e_id)."'>Folios</a>";
        })
        ->addColumn('valoracion', function($empleado){
            return number_format($empleado->valoracion,2);
        })
        ->rawColumns(['nombre','valoracion', 'btn'])
        ->toJson();
    }

    public function crear()
    {
        return view('forms.empleados',[
            'form_edit' =>false,
            'menu'      =>'empleados',
            'submenu'   =>''
        ]);
    }

    public function editar($id_empleado)
    {
        $empleado = Empleados::find($id_empleado);
        $usuario = User::where('empleado', '=', $id_empleado)->first();
        return view('forms.empleados',[
            'form_edit' => true,
            'menu'      => 'empleados',
            'submenu'   => '',
            'empleado'  => $empleado,
            'usuario'   => $usuario,
            'rol'       => $usuario->roles()->first()->name
        ]);
    }

    public function guardar(Request $request)
    {
        $this->validate($request,[
            'nombre'    =>'required',
            'paterno'   =>'required',
            'materno'   =>'required',
            'telefono'  =>'required|numeric',
            'email'     =>'required|string|email|unique:users,email',
			'password'	=>'required|string',
            'role'      =>'requerido'
        ],
        [
            'nombre.required'   =>'Ingrese el nombre del empleado',
            'paterno.required'  =>'Ingrese el apellido paterno del empleado',
            'materno.required'  =>'Ingrese el apellido materno del empleado',
            'telefono.required' =>'Ingrese un numero de contacto para el empleado',
            'telefono.required' =>'El telefono tiene que ser en formato numerico',
            'email.required'    =>'El correo es requerido',
			'email.email'       =>'El correo debe tener un formato válido',
			'email.unique'		=>'El correo ya está en uso, intente con otro',
            'password.required' =>'La contraseña es requerida',
            'rol.required'      =>'Indique el rol del empleado'
        ]);
        
        $empleado = Empleados::create([
            'nombre' => $request->nombre,
            'paterno' => $request->paterno,
            'materno' => $request->materno,
            'telefono' => $request->telefono
        ]);

        $user = User::create([
            'empleado' 	=> $empleado->id,
            'email' 	=> $request->email,
            'password'	=> bcrypt($request->password)]);
        
        switch($request->rol)
        {
            case 1:
                $user->assignRole('administrador');
                break;
            case 2:
                $user->assignRole('notario');
                break;
            case 3:
                $user->assignRole('capturista');
                break;
        }

        return response()->json([
            'status'=>  'true',
            'type'  =>  'success',
            'title' =>  'Éxito',
            'text'  =>  'Se han guardado los datos del empleado',
            'load'  =>  true,
            'url'   =>  route('empleados')
        ], 200);
    }

    public function actualizar(Request $request, $id_empleado)
    {
        $this->validate($request,[
            'nombre'    =>'required',
            'paterno'    =>'required',
            'materno'    =>'required',
            'telefono'    =>'required|numeric',
        ],
        [
            'nombre.required'   =>'Ingrese el nombre del empleado',
            'paterno.required' =>'Ingrese el apellido paterno del empleado',
            'materno.required'   =>'Ingrese el apellido materno del empleadocliente',
            'telefono.required'   =>'Ingrese un numero de contacto para el empleado',
            'telefono.required'   =>'El telefono tiene que ser en formato numerico',
        ]);

        $empleado = Empleados::find($id_empleado);
        $empleado->nombre = $request->nombre;
        $empleado->paterno = $request->paterno;
        $empleado->materno = $request->materno;
        $empleado->telefono = $request->telefono;
        $empleado->save();
        return response()->json([
            'status'=>  'true',
            'type'  =>  'success',
            'title' =>  'Éxito',
            'text'  =>  'Se ha actualizado los datos del empleado',
            'load'  =>  true,
            'url'   =>  route('empleados')
        ], 200);
    }

    public function expedientes($id_empleado)
    {
        return view('expediente',[
            'menu'      =>'empleados',
            'submenu'   =>'',
            'empleado'  =>$id_empleado
        ]);
    }

    public function datatable_exp(Request $request, $id_empleado)
    {
        $expediente = Expedientes::select('expedientes.id', 'asuntos.nombre as asunto', DB::raw('concat(clientes.nombre, " ", clientes.paterno, " ", clientes.materno) as cliente'), 'expedientes.propio', 'expedientes.status', DB::raw('DATE_FORMAT(expedientes.created_at, "%d/%m/%Y") as fecha'), 'asuntos.valoracion as valoracion')
                ->join('asuntos', 'asuntos.id','=', 'expedientes.asunto')
                ->join('clientes', 'clientes.id','=', 'expedientes.cliente')
                ->where('responsable', '=', $id_empleado);
        return DataTables::of($expediente)
        ->addColumn('propio', function($expediente){
            if($expediente->propio)
            {
                return "Externo";
            }else{
                return "Notaria";
            }
        })
        ->addColumn('status', function($expediente){
            if($expediente->status)
            {
                return "En proceso";
            }else{
                return "Finalizado";
            }
        })
        ->addColumn('btn', function($expediente){
            return "<a class='btn btn-primary text-white p-3 botones-exp' href='".route('generales',$expediente->id)."'>Generales</a><br>
                <a class='btn btn-warning text-white p-3 botones-exp' href='".route('impuestos_derechos',$expediente->id)."'>Impuesto y derechos</a><br>
                <a class='btn btn-danger text-white p-3 botones-exp' href='".route('otros',$expediente->id)."'>Otros</a>";
        })
        ->addColumn('next', function($expediente){
            return "<a class='btn btn-info text-white p-3 botones-exp' href='".route('seguimiento',$expediente->id)."'>Seguimiento</a><br>
            <a class='btn btn-warning text-white p-3 botones-exp' href='".route('impuestos_derechos',$expediente->id)."'>Folios</a><br>
            <a class='btn btn-success text-white p-3 botones-exp' href='".route('impuestos_derechos',$expediente->id)."'>Recordatorios</a>";
        })
        ->rawColumns(['btn', 'next'])
        ->toJson();
    }
}
