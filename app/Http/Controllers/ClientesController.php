<?php

namespace App\Http\Controllers;

use DataTables;
use App\Models\Clientes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class ClientesController extends Controller
{
    public function index()
    {
        return view('clientes',[
            'menu'      =>'clientes',
            'submenu'   =>''
        ]);

    }

    public function datatable(Request $request)
    {
        $cliente = Clientes::all();
        return DataTables::of($cliente)
        ->addColumn('nombre', function($cliente){
            if($cliente->razon_social == "Física")
            {
                return $cliente->paterno." ".$cliente->materno." ".$cliente->nombre;
            }else{
                return $cliente->nombre;
            }
        })
        ->addColumn('btn', function($cliente){
            return "<a class='btn btn-success text-white p-3 botones-exp' href='".route('editar_cliente',$cliente->id)."'>Editar</a>";
        })
        ->rawColumns(['nombre', 'btn'])
        ->toJson();
    }

    public function crear()
    {
        return view('forms.clientes',[
            'form_edit' =>false,
            'menu'      =>'clientes',
            'submenu'   =>''
        ]);
    }

    public function editar($id_cliente)
    {
        $cliente = Clientes::find($id_cliente);
        return view('forms.clientes',[
            'form_edit' =>true,
            'menu'      =>'clientes',
            'submenu'   =>'',
            'cliente' =>$cliente,
        ]);
    }

    public function guardar(Request $request)
    {
        if($request->tipo == 1)
        {
            $this->validate($request,[
                'nombre'    =>'required',
                'paterno'   =>'required',
                'materno'   =>'required',
                'curp'      =>'required|min:18|max:18',
                'rfc'       =>'required|min:13|max:13',
                'telefono'  =>'required|numeric',
            ],
            [
                'nombre.required'       =>'Ingrese el nombre del cliente',
                'paterno.required'      =>'Ingrese el apellido paterno del cliente',
                'materno.required'      =>'Ingrese el apellido materno del cliente',
                'curp.required'         =>'Ingrese el CURP del cliente',
                'curp.min'              =>'Formato no valido para la CURP',
                'curp.max'              =>'Formato no valido para la CURP',
                'rfc.required'          =>'Ingrese el RFC del cliente',
                'rfc.min'               =>'Formato no valido para el RFC',
                'rfc.max'               =>'Formato no valido para el RFC',
                'telefono.required'     =>'Ingrese un numero de contacto para el cliente',
                'telefono.numeric'      =>'El telefono tiene que ser en formato numerico',
            ]);
        }else{
            $this->validate($request,[
                'nombre'        =>'required',
                'rfc'           =>'required|min:12|max:12',
                'telefono'      =>'required|numeric',
            ],
            [
                'nombre.required'       =>'Ingrese el nombre de la empresa',
                'rfc.required'          =>'Ingrese el RFC del cliente',
                'rfc.min'               =>'Formato no valido para el RFC',
                'rfc.max'               =>'Formato no valido para el RFC',
                'telefono.required'     =>'Ingrese un numero de contacto',
                'telefono.numeric'      =>'El telefono tiene que ser en formato numerico',
            ]);
        }

        if($request->tipo == 1)
        {
            $cliente = Clientes::create([
                'nombre'    => $request->nombre,
                'paterno'   => $request->paterno,
                'materno'   => $request->materno,
                'RFC'       => $request->rfc,
                'CURP'      => $request->curp,
                'telefono'  => $request->telefono
            ]);
        }else{
            $cliente = Clientes::create([
                'razon_social'  => "Moral",
                'nombre'        => $request->nombre,
                'RFC'           => $request->rfc,
                'telefono'      => $request->telefono
            ]);
        }
        return response()->json([
            'status'=>  'true',
            'type'  =>  'success',
            'title' =>  'Éxito',
            'text'  =>  'Se han guardado los datos del cliente',
            'load'  =>  true,
            'url'   =>  route('clientes')
        ], 200);
    }

    public function actualizar(Request $request, $id_cliente)
    {
        if($request->tipo == 1)
        {
            $this->validate($request,[
                'nombre'    =>'required',
                'paterno'    =>'required',
                'materno'    =>'required',
                'telefono'    =>'required|numeric',
            ],
            [
                'nombre.required'   =>'Ingrese el nombre del cliente',
                'paterno.required'  =>'Ingrese el apellido paterno del cliente',
                'materno.required'  =>'Ingrese el apellido materno del cliente',
                'telefono.required' =>'Ingrese un numero de contacto para el cliente',
                'telefono.numeric'  =>'El telefono tiene que ser en formato numerico',
            ]);
        }else{
            $this->validate($request,[
                'nombre'        =>'required',
                'telefono'      =>'required|numeric',
            ],
            [
                'nombre.required'       =>'Ingrese el nombre de la empresa',
                'telefono.required'     =>'Ingrese un numero de contacto',
                'telefono.numeric'      =>'El telefono tiene que ser en formato numerico',
            ]);
        }

        $cliente = Clientes::find($id_cliente);
        if($request->tipo == 1)
        {
            $cliente->razon_social = "Física";
            $cliente->nombre = $request->nombre;
            $cliente->paterno = $request->paterno;
            $cliente->materno = $request->materno;
            $cliente->telefono = $request->telefono;
        }else{
            $cliente->razon_social = "Moral";
            $cliente->nombre = $request->nombre;
            $cliente->telefono = $request->telefono;
        }
        $cliente->save();
        
        return response()->json([
            'status'=>  'true',
            'type'  =>  'success',
            'title' =>  'Éxito',
            'text'  =>  'Se ha actualizado los datos del cliente',
            'load'  =>  true,
            'url'   =>  route('clientes')
        ], 200);
    }

    public function listar_tipo(Request $request)
    {
        $this->validate($request,[
            'tipo'  =>'required',
        ],
        [
            'tipo.required'   =>'Indique si es una persona física o moral',
        ]);

        if($request->tipo == 1)
        {
            $cliente = Clientes::where("razon_social", "=" , "Física")->get();
            $i=1;
            $datos = "";
            foreach($cliente as $item)
            {
                if($i == 1)
                {
                    $datos .= "<option value='".$item->id."' selected>".$item->paterno." ".$item->materno." ".$item->nombre."</option>";
                }else{
                    $datos .= "<option value='".$item->id."'>".$item->paterno." ".$item->materno." ".$item->nombre."</option>";
                }
                $i++;
            }
            return $datos;
        }else{
            $cliente = Clientes::where("razon_social", "=" , "Moral")->get();
            $i=1;
            $datos = "";
            foreach($cliente as $item)
            {
                if($i == 1)
                {
                    $datos .= "<option value='".$item->id."' selected>".$item->nombre."</option>";
                }else{
                    $datos .= "<option value='".$item->id."'>".$item->nombre."</option>";
                }
                $i++;
            }
            return $datos;
        }


    }
}
