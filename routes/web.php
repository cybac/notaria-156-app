<?php

use Illuminate\Support\Facades\Route;

Auth::routes();
Route::get('/', 'HomeController@welcome')->name('index');

Route::group(['middleware' => ['auth']], function() {
    Route::prefix('notaria')->group(function () {
        Route::get('/', 'HomeController@index')->name('notaria');

        Route::prefix('perfil')->group(function () {
            Route::get('/','PerfilController@index')->name('perfil');
            Route::get('password','PerfilController@password')->name('password');
            Route::post('actualiar','PerfilController@actualizar')->name('actualizar_perfil');
            Route::post('actualizar_password','PerfilController@actualizar_pass')->name('actualizar_password');
        });

        Route::prefix('expedientes')->group(function () {
            Route::get('/','ExpedientesController@index')->name('expedientes');
            Route::post('datatable','ExpedientesController@datatable')->name('dt_expedientes');
            Route::get('crear','ExpedientesController@crear')->name('crear_expediente');
            Route::post('guardar','ExpedientesController@guardar')->name('guardar_expediente');
            
            Route::prefix('documentos')->group(function () {
                Route::prefix('generales')->group(function () {
                    Route::get('/{id_expediente}','DocumentosController@documentos_generales')->name('generales');
                    Route::get('editar/{id_documento}','DocumentosController@editar')->name('editar_general');
                    Route::post('actualizar','DocumentosController@actualizar')->name('actualizar_general');
                });

                Route::prefix('impuestos_derechos')->group(function () {
                    Route::get('/{id_expediente}','DocumentosController@documentos_id')->name('impuestos_derechos');
                    Route::get('subir/{id_expediente}','DocumentosController@subir_id')->name('crear_impuestos_derechos');
                });

                Route::prefix('otros')->group(function () {
                    Route::get('/{id_expediente}','DocumentosController@documentos_otros')->name('otros');
                    Route::get('subir/{id_expediente}','DocumentosController@subir_otro')->name('crear_otro');
                });
                
                Route::post('datatable/{criterio}/{id_documento}','DocumentosController@datatable')->name('dt_documentos');
                Route::post('guardar/{tipo}','DocumentosController@guardar')->name('guardar_documento');
                Route::post('actualizar/{id_documento}','DocumentosController@actualizar')->name('actualizar_documento');
            });

            Route::prefix('seguimiento')->group(function () {
                Route::get('/{id_expediente}','SeguimientoController@index')->name('seguimiento');
                Route::post('datatable/{id_expediente}','SeguimientoController@datatable')->name('dt_seguimiento');
                Route::get('crear/{id_expediente}','SeguimientoController@crear')->name('crear_seguimiento');
                Route::post('guardar/{id_expediente}','SeguimientoController@guardar')->name('guardar_seguimiento');
            });
    
            Route::prefix('folios')->group(function () {
                Route::get('/{id_expediente}','FoliosController@index')->name('folios');
                Route::post('datatable/{id_expediente}','FoliosController@datatable')->name('dt_folios');
                Route::get('crear/{id_expediente}','FoliosController@crear')->name('crear_folio');
                Route::post('guardar{id_expediente}','FoliosController@guardar')->name('guardar_folio');
            });

            Route::prefix('recordatorios')->group(function () {
                Route::get('/{id_expediente}','RecordatoriosController@index')->name('recordatorios');
                Route::post('datatable/{id_expediente}','RecordatoriosController@datatable')->name('dt_recordatorios');
                Route::get('crear/{id_expediente}','RecordatoriosController@crear')->name('crear_recordatorios');
                Route::post('guardar{id_expediente}','RecordatoriosController@guardar')->name('guardar_recordatorio');
            });
        });
    
        Route::prefix('clientes')->group(function () {
            Route::get('/','ClientesController@index')->name('clientes');
            Route::post('datatable','ClientesController@datatable')->name('dt_clientes');
            Route::get('crear','ClientesController@crear')->name('crear_cliente');
            Route::get('editar/{id_cliente}','ClientesController@editar')->name('editar_cliente');
            Route::post('guardar','ClientesController@guardar')->name('guardar_cliente');
            Route::post('actualizar/{id_cliente}','ClientesController@actualizar')->name('actualizar_cliente');
            Route::post('listado_tipo','ClientesController@listar_tipo')->name('listar_cliente');
    
            Route::prefix('pagos')->group(function () {
                Route::get('/{id_cliente}','PagosController@index')->name('pagos');
                Route::post('datatable/{id_cliente}','PagosController@datatable')->name('dt_pagos');
                Route::get('crear/{id_cliente}','PagosController@crear')->name('crear_pago');
                Route::post('guardar','PagosController@guardar')->name('guardar_pago');
                Route::get('editar/{id_pago}','PagosController@editar')->name('editar_pago');
                Route::post('actualizar/{id_pago}','PagosController@actualizar')->name('actualizar_pago');
            });
        });

        Route::group(['middleware' => ['role:administrador']], function () {
            Route::prefix('empleados')->group(function () {
                Route::get('/','EmpleadosController@index')->name('empleados');
                Route::post('datatable','EmpleadosController@datatable')->name('dt_empleados');
                Route::get('crear','EmpleadosController@crear')->name('crear_empleado');
                Route::get('editar/{id_empleado}','EmpleadosController@editar')->name('editar_empleado');
                Route::post('guardar','EmpleadosController@guardar')->name('guardar_empleado');
                Route::post('actualizar/{id_empleado}','EmpleadosController@actualizar')->name('actualizar_empleado');

                Route::prefix('expedientes')->group(function () {
                    Route::get('/{id_empleado}','EmpleadosController@expedientes')->name('expedientes_empleado');
                    Route::post('datatable/{id_empleado}','EmpleadosController@datatable_exp')->name('dt_expediente');
                });
                
            });

            Route::prefix('asuntos')->group(function () {
                Route::get('/','AsuntosController@index')->name('asuntos');
                Route::post('datatable','AsuntosController@datatable')->name('dt_asuntos');
                Route::get('crear','AsuntosController@crear')->name('crear_asunto');
                Route::get('editar/{id_asunto}','AsuntosController@editar')->name('editar_asunto');
                Route::post('guardar','AsuntosController@guardar')->name('guardar_asunto');
                Route::post('actualizar/{id_asunto}','AsuntosController@actualizar')->name('actualizar_asunto');
            });

            Route::prefix('listar_folios')->group(function () {
                Route::get('/','FoliosController@all')->name('listar_folios');
                Route::post('datatable','FoliosController@datatable_folios')->name('dt_listar_folios');
            });

            Route::prefix('folios_empleado')->group(function () {
                Route::get('/{id_empleado}','FoliosController@folios_empleado')->name('folios_empleado');
                Route::post('datatable/{id_empleado}','FoliosController@datatable_folios_empleado')->name('dt_listar_folios_empleado');
            });
        });
    });
});