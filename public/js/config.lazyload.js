// lazyload config
var MODULE_CONFIG = {
    easyPieChart:   [ '../js/libs/jquery/jquery.easy-pie-chart/dist/jquery.easypiechart.fill.js' ],
    sparkline:      [ '../js/libs/jquery/jquery.sparkline/dist/jquery.sparkline.retina.js' ],
    plot:           [ '../js/libs/jquery/flot/jquery.flot.js',
                      '../js/libs/jquery/flot/jquery.flot.resize.js',
                      '../js/libs/jquery/flot/jquery.flot.pie.js',
                      '../js/libs/jquery/flot.tooltip/js/jquery.flot.tooltip.min.js',
                      '../js/libs/jquery/flot-spline/js/jquery.flot.spline.min.js',
                      '../js/libs/jquery/flot.orderbars/js/jquery.flot.orderBars.js'],
    vectorMap:      [ '../js/libs/jquery/bower-jvectormap/jquery-jvectormap-1.2.2.min.js',
                      '../js/libs/jquery/bower-jvectormap/jquery-jvectormap.css', 
                      '../js/libs/jquery/bower-jvectormap/jquery-jvectormap-world-mill-en.js',
                      '../js/libs/jquery/bower-jvectormap/jquery-jvectormap-us-aea-en.js' ],
    dataTable:      [
                      '../js/libs/jquery/datatables/media/js/jquery.dataTables.min.js',
                      '../js/libs/jquery/plugins/integration/bootstrap/3/dataTables.bootstrap.js',
                      '../js/libs/jquery/plugins/integration/bootstrap/3/dataTables.bootstrap.css'],
    footable:       [
                      '../js/libs/jquery/footable/dist/footable.all.min.js',
                      '../js/libs/jquery/footable/css/footable.core.css'
                    ],
    screenfull:     [
                      '../js/libs/jquery/screenfull/dist/screenfull.min.js'
                    ],
    sortable:       [
                      '../js/libs/jquery/html.sortable/dist/html.sortable.min.js'
                    ],
    nestable:       [
                      '../js/libs/jquery/nestable/jquery.nestable.css',
                      '../js/libs/jquery/nestable/jquery.nestable.js'
                    ],
    summernote:     [
                      '../js/libs/jquery/summernote/dist/summernote.css',
                      '../js/libs/jquery/summernote/dist/summernote.js'
                    ],
    parsley:        [
                      '../js/libs/jquery/parsleyjs/dist/parsley.css',
                      '../js/libs/jquery/parsleyjs/dist/parsley.min.js'
                    ],
    select2:        [
                      '../js/libs/jquery/select2/dist/css/select2.min.css',
                      '../js/libs/jquery/select2-bootstrap-theme/dist/select2-bootstrap.min.css',
                      '../js/libs/jquery/select2-bootstrap-theme/dist/select2-bootstrap.4.css',
                      '../js/libs/jquery/select2/dist/js/select2.min.js'
                    ],
    datetimepicker: [
                      '../js/libs/jquery/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css',
                      '../js/libs/jquery/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.dark.css',
                      '../js/libs/js/moment/moment.js',
                      '../js/libs/jquery/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js'
                    ],
    chart:          [
                      '../js/libs/js/echarts/build/dist/echarts-all.js',
                      '../js/libs/js/echarts/build/dist/theme.js',
                      '../js/libs/js/echarts/build/dist/jquery.echarts.js'
                    ],
    bootstrapWizard:[
                      '../js/libs/jquery/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js'
                    ],
    fullCalendar:   [
                      '../js/libs/jquery/moment/moment.js',
                      '../js/libs/jquery/fullcalendar/dist/fullcalendar.min.js',
                      '../js/libs/jquery/fullcalendar/dist/fullcalendar.css',
                      '../js/libs/jquery/fullcalendar/dist/fullcalendar.theme.css',
                      '../js/plugins/calendar.js'
                    ],
    dropzone:       [
                      '../js/libs/js/dropzone/dist/min/dropzone.min.js',
                      '../js/libs/js/dropzone/dist/min/dropzone.min.css'
                    ]
  };
