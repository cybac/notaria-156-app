var paterno = "", materno = "", curp = "";
$(document).ready(function () {
    $("#tipo").on("change",function() {
        if($("#tipo").val() == 1)
        {
            $("#nombre_cliente").text("Nombre ");
            $("#nombre_cliente").append("<span class='text-danger'>*</span>");
            $("#pat")[0].classList.remove("d-none");
            $("#mat")[0].classList.remove("d-none");
            $("#cur")[0].classList.remove("d-none");
            $("#nom")[0].classList.remove("col-md-12");
            $("#rf")[0].classList.remove("col-md-12");
            $("#tel")[0].classList.remove("col-md-12");
            $("#pat").append("\
                <label for='paterno'>Paterno <span class='text-danger'>*</span></label>\
                <input type='text' class='form-control form-control-lg' name='paterno' id='paterno' value='"+paterno+"'>\
                <span class='invalid-feedback'></span>");
            $("#mat").append("\
                <label for='materno'>Materno <span class='text-danger'>*</span></label>\
                <input type='text' class='form-control form-control-lg' name='materno' id='materno' value='"+materno+"'>\
                <span class='invalid-feedback'></span>");
            $("#cur").append("\
                <label for='curp'>CURP <span class='text-danger'>*</span></label>\
                <input type='text' class='form-control form-control-lg' name='curp' id='curp' value='"+curp+"'>\
                <span class='invalid-feedback'></span>\
                <script src='"+window.location.origin +"/js/mayusculas.js'></script>");
            $("#form_cliente").find('.is-invalid').removeClass('is-invalid');
        }else{
            paterno = $("#paterno").val();
            materno = $("#materno").val();
            curp = $("#curp").val();
            $("#nombre_cliente").text("Nombre de la empresa ");
            $("#nombre_cliente").append("<span class='text-danger'>*</span>");
            $("#pat")[0].classList.add("d-none");
            $("#mat")[0].classList.add("d-none");
            $("#cur")[0].classList.add("d-none");
            $("#nom")[0].classList.add("col-md-12");
            $("#rf")[0].classList.add("col-md-12");
            $("#tel")[0].classList.add("col-md-12");
            $("#pat").empty();
            $("#mat").empty();
            $("#cur").empty();
            $("#form_cliente").find('.is-invalid').removeClass('is-invalid');
        }
    });
});

