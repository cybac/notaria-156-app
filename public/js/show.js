var password, icon, activo = true;

$("#show" ).click(function() {
    password =document.getElementById("password");

    if(activo)
    {
        $("#show-icon").removeClass("far fa-eye").addClass("far fa-eye-slash");
        password.type = "text";
        activo = false;
    }else{
        $("#show-icon").removeClass("far fa-eye-slash").addClass("far fa-eye");
        password.type = "password";
        activo = true;

    }
});