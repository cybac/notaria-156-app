$(document).ready(function () {
    $("#tipo").on("change",function() {
        if($("#tipo").val() == 1)
        {
            $("#cliente").empty();
            CargarClientes($("#tipo").val());
        }else{
            $("#cliente").empty();
            CargarClientes($("#tipo").val());
        }
    });

    function CargarClientes(tipo)
    {
        $.ajax({
            url: Urldestino = window.location.origin + "/notaria/clientes/listado_tipo",
            type: "POST",
            data: {
                _token: $('meta[name="csrf-token"]').attr('content'),
                "tipo":tipo
            },
        }).done(function (response) {
            console.log(response);
            $("#cliente").append(response);
        }).fail(function (response) {
            console.log("Error");
            console.log(response);
        });
    }
});

