$(document).ready(function () {
    $('#dt_expedientes').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: $("#dt_expedientes").data('url'),
            dataType: "json",
            type: "POST",
            data: {
                _token: $("meta[name='csrf-token'] ").attr('content')
            }
        },
        language: {
            url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        columns: [{
            data: "asunto",
        }, {
            data: "cliente"
        }, {
            data: "responsable"
        }, {
            data: "propio"
        }, {
            data: "fecha",
        }, {
            data: "status",
        }, {
            data: "btn",
            "orderable": false
        }, {
            data: "next",
            "orderable": false
        }]
    });

    $('#dt_expediente').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: $("#dt_expediente").data('url'),
            dataType: "json",
            type: "POST",
            data: {
                _token: $("meta[name='csrf-token'] ").attr('content')
            }
        },
        language: {
            url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        columns: [{
            data: "asunto",
        }, {
            data: "cliente"
        }, {
            data: "propio"
        }, {
            data: "fecha",
        }, {
            data: "status",
        }, {
            data: "valoracion"
        }, {
            data: "btn",
            "orderable": false
        }, {
            data: "next",
            "orderable": false
        }]
    });


    $('#dt_folios').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: $("#dt_folios").data('url'),
            dataType: "json",
            type: "POST",
            data: {
                _token: $("meta[name='csrf-token'] ").attr('content')
            }
        },
        language: {
            url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        columns: [{
            data: "escritura",
        }, {
            data: "volumen"
        }, {
            data: "folio",
        }, {
            data: "actos"
        }, {
            data: "partes",
            "orderable": false
        }, {
            data: "notas",
            "orderable": false
        }, {
            data: "fecha"
        }]
    });

    $('#dt_listar_folios').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: $("#dt_listar_folios").data('url'),
            dataType: "json",
            type: "POST",
            data: {
                _token: $("meta[name='csrf-token'] ").attr('content')
            }
        },
        language: {
            url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        columns: [{
            data: "escritura",
        }, {
            data: "volumen"
        }, {
            data: "folio",
        }, {
            data: "actos"
        }, {
            data: "partes",
            "orderable": false
        }, {
            data: "notas",
            "orderable": false
        }, {
            data: "empleado"
        }, {
            data: "fecha"
        }]
    });

    $('#dt_listar_folios_empleado').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: $("#dt_listar_folios_empleado").data('url'),
            dataType: "json",
            type: "POST",
            data: {
                _token: $("meta[name='csrf-token'] ").attr('content')
            }
        },
        language: {
            url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        columns: [{
            data: "escritura",
        }, {
            data: "volumen"
        }, {
            data: "folio",
        }, {
            data: "actos"
        }, {
            data: "partes",
            "orderable": false
        }, {
            data: "notas",
            "orderable": false
        }, {
            data: "fecha"
        }]
    });

    $('#dt_recordatorios').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: $("#dt_recordatorios").data('url'),
            dataType: "json",
            type: "POST",
            data: {
                _token: $("meta[name='csrf-token'] ").attr('content')
            }
        },
        language: {
            url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        columns: [{
            data: "titulo",
        }, {
            data: "contenido"
        }, {
            data: "fecha",
        }, {
            data: "status",
        }, {
            data: "btn",
            "orderable": false
        }]
    });



    $('#dt_documentos').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: $("#dt_documentos").data('url'),
            dataType: "json",
            type: "POST",
            data: {
                _token: $("meta[name='csrf-token'] ").attr('content')
            }
        },
        language: {
            url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        columns: [{
            data: "alias",
        }, {
            data: "fecha",
        }, {
            data: "status",
        }, {
            data: "btn",
            "orderable": false
        }]
    });

    $('#dt_seguimiento').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: $("#dt_seguimiento").data('url'),
            dataType: "json",
            type: "POST",
            data: {
                _token: $("meta[name='csrf-token'] ").attr('content')
            }
        },
        language: {
            url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        columns: [{
            data: "titulo"
        }, {
            data: "descripcion"
        }, {
            data: "fecha"
        }]
    });

    $('#dt_clientes').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: $("#dt_clientes").data('url'),
            dataType: "json",
            type: "POST",
            data: {
                _token: $("meta[name='csrf-token'] ").attr('content')
            }
        },
        language: {
            url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        columns: [{
            data: "nombre",
        }, {
            data: "telefono"
        }, {
            data: "razon_social",
        }, {
            data: "btn",
            "orderable": false
        }]
    });

    $('#dt_empleados').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: $("#dt_empleados").data('url'),
            dataType: "json",
            type: "POST",
            data: {
                _token: $("meta[name='csrf-token'] ").attr('content')
            }
        },
        language: {
            url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        columns: [{
            data: "nombre"
        }, {
            data: "email",
        }, {
            data: "carga",
        }, {
            data: "valoracion",
        }, {
            data: "btn",
            "orderable": false
        }]
    });

    $('#dt_pagos').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: $("#dt_pagos").data('url'),
            dataType: "json",
            type: "POST",
            data: {
                _token: $("meta[name='csrf-token'] ").attr('content')
            }
        },
        language: {
            url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        columns: [{
            data: "nombre",
        }, {
            data: "monto"
        }, {
            data: "descripcion"
        }, {
            data: "fecha",
        }, {
            data: "btn",
            "orderable": false
        }]
    });

    $('#dt_asuntos').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: $("#dt_asuntos").data('url'),
            dataType: "json",
            type: "POST",
            data: {
                _token: $("meta[name='csrf-token'] ").attr('content')
            }
        },
        language: {
            url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        columns: [{
            data: "nombre",
        }, {
            data: "valoracion"
        }, {
            data: "btn",
            "orderable": false
        }]
    });

});
