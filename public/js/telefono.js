window.addEventListener("load", function () {
    telefono.addEventListener("keypress", soloNumeros, false);
    telefono.onpaste = function(e) {
        e.preventDefault();
        M.toast({html: "Esta acción está prohibida"});
      }
    telefono.oncopy = function(e) {
        e.preventDefault();
        M.toast({html: "Esta acción está prohibida"});
      }
});

//Solo permite introducir numeros.
function soloNumeros(e) {
    var key = window.event ? e.which : e.keyCode;
    if (key < 48 || key > 57) {
        e.preventDefault();
    }
    if (telefono.value.length > 9) {
        e.preventDefault();
    }
}
