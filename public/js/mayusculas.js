var max_char = 12;
$(document).ready(function () {
    $("#rfc").on("keyup",function() {
        $("#rfc").val($("#rfc").val().toUpperCase());
    });

    $("#rfc").on("keypress",function(e) {
        if($("#tipo").val() == 1)
        {
            max_char = 12;
        }else{
            max_char = 11;
        }
        if ($("#rfc").val().length > max_char) {
            e.preventDefault();
        }
    });

    $("#curp").on("keyup",function() {
        $("#curp").val($("#curp").val().toUpperCase());
    });

    $("#curp").on("keypress",function(e) {
        if ($("#curp").val().length > 17) {
            e.preventDefault();
        }
    });

    $("#rfc").on("paste",function(e) {
        e.preventDefault();
        M.toast({html: "El pegar contenido está prohibido"});
    });
    
    $("#curp").on("paste",function(e) {
        e.preventDefault();
        M.toast({html: "El pegar contenido está prohibido"});
    });
    
    $("#rfc").on("copy",function(e) {
        e.preventDefault();
        M.toast({html: "El copiar contenido está prohibido"});
    });

    $("#curp").on("copy",function(e) {
        e.preventDefault();
        M.toast({html: "El copiar contenido está prohibido"});
    });

});

